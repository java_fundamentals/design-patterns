package com.design.creational.singleton.enumsingleton;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class EnumSingletonTest {
    public static void main(String[] args) {
        // 获取单例对象
        EnumSingleton instance1 = EnumSingleton.INSTANCE;
        EnumSingleton instance2 = EnumSingleton.INSTANCE;

        log.info("instance1 = {}", instance1);
        log.info("instance2 = {}", instance2);
        log.info("instance1.equals(instance2) = {}", instance1.equals(instance2));
        log.info("instance1.hashCode() = {}", instance1.hashCode());
        log.info("instance2.hashCode() = {}", instance2.hashCode());
        log.info("instance1 == instance2: {}", instance1 == instance2);
    }
}
