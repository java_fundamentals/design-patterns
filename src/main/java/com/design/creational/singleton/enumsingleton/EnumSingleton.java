package com.design.creational.singleton.enumsingleton;

/**
 * 枚举式单例模式 属于饿汉式
 *
 *  枚举类实现单例模式是Java语言中最安全的一种方式。
 *  这种写法是Effective Java作者Josh Bloch 提倡的方式，它更简洁，
 *  无偿地提供了序列化机制，绝对防止多次实例化。而且枚举类型是所有单例实现中唯一一种
 *  不会被破坏的单例实现模式。
 *
 *  虽然这种实现方式还没有广泛采用，但只要occurs-before原则得到保证，
 *  它比饿汉式单例模式具有更好的性能，尤其是延迟加载方面。
 *
 *  结论：极力推荐使用。
 *
 */
public enum EnumSingleton {
    INSTANCE;
}
