package com.design.creational.singleton.hungrysingleton;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class HungrySingletonThreadTest {
    public static void main(String[] args) {
        new Thread(()->{
            HungrySingleton hungrySingleton = HungrySingleton.getInstance();
            log.info(hungrySingleton);
        }).start();

        new Thread(()->{
            HungrySingleton hungrySingleton = HungrySingleton.getInstance();
            log.info(hungrySingleton);
        }).start();
    }
}
