package com.design.creational.singleton.hungrysingleton;

/**
 * 饿汉式单例模式：静态代码块
 *
 */
public class HungrySingleton2 {
    /**
     *  私有构造方法
     */
    private HungrySingleton2() {
    }

    /**
     *  在类加载时就初始化
     *  静态属性
     *  线程安全
     *  效率高
     *  不能延时加载
     *  可能造成内存浪费
     */
    private static final HungrySingleton2 INSTANCE;

    /**
     *  在静态代码块中初始化
     *  JVM保证线程安全
     *  可能造成内存浪费
     *  优点：在类加载时就初始化，避免线程同步问题
     *  缺点：可能造成内存浪费
     *  使用场景：在单例模式中必须使用时才创建实例
     */
    static {
        INSTANCE = new HungrySingleton2();
    }

    /**
     * 对外提供获取实例的静态方法
     * @return
     */
    public static HungrySingleton2 getInstance() {
        return INSTANCE;
    }
}
