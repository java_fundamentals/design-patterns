package com.design.creational.singleton.hungrysingleton;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class HungrySingletonTest {
    public static void main(String[] args) {
        HungrySingleton hungrySingleton1 = HungrySingleton.getInstance();
        HungrySingleton hungrySingleton2 = HungrySingleton.getInstance();
        log.info(hungrySingleton1 == hungrySingleton2);
    }
}
