package com.design.creational.singleton.hungrysingleton;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class HungrySingletonTest2 {
    public static void main(String[] args) {
        // 测试饿汉式单例模式
        HungrySingleton2 instance1 = HungrySingleton2.getInstance();
        HungrySingleton2 instance2 = HungrySingleton2.getInstance();
        log.info("instance1: {}", instance1);
        log.info("instance2: {}", instance2);
        log.info("instance1 == instance2: {}", instance1 == instance2);
    }
}
