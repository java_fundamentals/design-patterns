package com.design.creational.singleton.lazysingleton;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class LazySingletonThreadTest {
    public static void main(String[] args) {
        new Thread(()->{
            LazySingleton lazySingleton = LazySingleton.getInstance();
            log.info(lazySingleton);
        }).start();

        new Thread(()->{
            LazySingleton lazySingleton = LazySingleton.getInstance();
            log.info(lazySingleton);
        }).start();
    }
}
