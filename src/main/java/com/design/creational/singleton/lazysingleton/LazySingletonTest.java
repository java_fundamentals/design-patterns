package com.design.creational.singleton.lazysingleton;

import lombok.extern.log4j.Log4j2;
@Log4j2
public class LazySingletonTest {
    public static void main(String[] args) {
        LazySingleton lazySingleton1 = LazySingleton.getInstance();
        LazySingleton lazySingleton2 = LazySingleton.getInstance();
        log.info(lazySingleton1 == lazySingleton2);
    }
}
