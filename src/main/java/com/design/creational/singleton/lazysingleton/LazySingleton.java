package com.design.creational.singleton.lazysingleton;

/**
 * 懒汉式单例模式（Lazy Initialization Singleton）是一种延迟实例化的单例模式。
 * 在这种模式下，实例对象在第一次被使用时才会被创建。
 * 下面是一个简单的懒汉式单例模式的示例实现
 * LazySingleton类只有一个私有的静态成员变量instance，用于保存唯一的实例对象。
 * getInstance()方法通过判断instance是否为null来决定是否创建新的实例。
 * 为了保证线程安全，在getInstance()方法上加了synchronized关键字，以保证在多线程环境下只有一个线程可以执行实例的创建。
 * 懒汉式单例模式的延迟实例化可能会引发线程安全问题，为了解决这个问题，使用了双重检查锁定的方式来实现更高效的懒汉式单例模式。
 * @return 单例对象的实例
 */
public class LazySingleton {
    /**
     * 使用了volatile关键字来确保多线程环境下的可见性和有序性。
     */
    private volatile static LazySingleton INSTANCE;
    private LazySingleton(){
        // 私有构造方法，防止外部实例化
    }

    /**
     * 在上述代码中，LazySingleton类只有一个私有的静态成员变量instance，用于保存唯一的实例对象。
     * getInstance()方法通过判断instance是否为null来决定是否创建新的实例。由于懒汉式单例模式是线程安全的，
     * 因此在getInstance()方法上加了synchronized关键字，以保证在多线程环境下只有一个线程可以执行实例的创建。
     *
     * 需要注意的是，懒汉式单例模式的延迟实例化可能会引发线程安全问题，尤其是在多线程环境下。为了解决这个问题，
     * 可以使用双重检查锁定（double-checked locking）或者静态内部类的方式来实现更高效的懒汉式单例模式。
     * @return
     */
    public static LazySingleton getInstance(){
        if (INSTANCE == null) {
            synchronized(LazySingleton.class){
                if (INSTANCE == null) {
                    INSTANCE = new LazySingleton();
                }
            }
        }
        return INSTANCE;
    }
}
