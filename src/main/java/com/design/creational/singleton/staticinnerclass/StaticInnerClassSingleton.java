package com.design.creational.singleton.staticinnerclass;

import java.io.Serializable;

/**
 * 懒汉式-静态内部类
 *
 * 优点：
 * 1、只有在使用的时候才会加载，实现了懒加载
 * 2、线程安全
 *
 * 缺点：
 * 1、可能会增加类的数量，因为需要一个静态内部类来保存单例对象
 * 2、第一次加载时可能会略微增加启动时间
 *
 * 总结来说，静态内部类单例是一种实现单例模式的优秀方式，它既保证了线程安全，又具有很好的类加载性能，并且使用起来非常方便。
 */
public class StaticInnerClassSingleton implements Serializable {
    /**
     *  私有构造器
     */
    private StaticInnerClassSingleton(){

    }

    /**
     *  静态内部类
     */
    private static class SingletonHolder{
        private static final StaticInnerClassSingleton INSTANCE = new StaticInnerClassSingleton();
    }

    /**
     *  提供全局访问点
     * @return
     */
    public static StaticInnerClassSingleton getInstance(){
        return SingletonHolder.INSTANCE;
    }

    /**
     *  序列化时，如果需要反序列化出单例对象，需要添加如下方法
     *
     *  如果没有这个方法，反序列化出来的对象不是同一个对象
     *
     *  readResolve()方法是JDK自带的，如果需要自定义反序列化出来的对象，可以重写这个方法
     *
     *  readResolve()方法会在反序列化时被调用，当反序列化结束时，会调用readResolve()方法来
     *  返回一个对象，这个对象会替代反序列化出来的对象
     *
     *  readResolve()方法返回的对象必须是单例对象
     *
     * @return
     */
    public  Object readResolve() {
        return SingletonHolder.INSTANCE;
    }
}
