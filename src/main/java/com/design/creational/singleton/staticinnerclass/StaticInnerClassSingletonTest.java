package com.design.creational.singleton.staticinnerclass;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class StaticInnerClassSingletonTest {
    public static void main(String[] args) {
        // 测试单例模式
        StaticInnerClassSingleton instance1 = StaticInnerClassSingleton.getInstance();
        StaticInnerClassSingleton instance2 = StaticInnerClassSingleton.getInstance();
        log.info("instance1: {}", instance1);
        log.info("instance2: {}", instance2);
        log.info("instance1 == instance2: {}", instance1 == instance2);
    }
}
