package com.design.creational.prototype.deepcloning;

import lombok.extern.log4j.Log4j2;

import java.io.Serializable;

/**
 * 奖状类
 */
@Log4j2
public class Citation implements Cloneable, Serializable {
    private Stutent stutent;

    public Stutent getStutent() {
        return stutent;
    }

    public Citation setStutent(Stutent stutent) {
        this.stutent = stutent;
        return this;
    }

    public void show(){
        log.info("{}同学：在2024学年第一学期中表现优秀，被评为三好学生。特发此状！",stutent.getName());
    }

    @Override
    protected Citation clone() throws CloneNotSupportedException {
        return (Citation) super.clone();
    }
}
