package com.design.creational.prototype.deepcloning;

import java.io.Serializable;

/**
 * 学生类
 */
public class Stutent implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public Stutent setName(String name) {
        this.name = name;
        return this;
    }
}
