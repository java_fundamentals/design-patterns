package com.design.creational.prototype.deepcloning;


import lombok.extern.log4j.Log4j2;
import java.io.*;

/**
 * <p>深克隆</p>
 * <p>实现方式：</p>
 * <p>	1. 复制对象的所有变量值。</p>
 * <p>	2. 为所有复制的变量赋新的引用地址。</p>
 * <p>	3. 返回新的对象。</p>
 * <p>  4. 实现Cloneable接口。</p>
 * <p>	5. 重写Object类中的clone()方法。</p>
 * <p>	6. 在clone()方法中，只是简单地调用super.clone()来实现对象复制。</p>
 * <p>	7. 实现Cloneable接口的类有一个clone()方法，在调用该方法时，会返回这个类的一个新的复本。</p>
 * <p>	8. 深克隆的实现需要采用流的形式读取对象的二进制数据，采用流的形式写入对象的二进制数据。</p>
 * <p>	9. 深克隆要求被复制对象的所有变量都是可复制的，即这些变量所对应的类都要实现Cloneable接口。</p>
 * <p>	10. 深克隆与浅克隆的区别在于，浅克隆只是复制了对象，并没有为对象的所有变量赋新的引用地址。</p>
 * <p>	11. 深克隆与浅克隆的实现都是调用Object类中的clone()方法。</p>
 * <p>	12. 深克隆要求实现clone()方法的对象必须实现Cloneable接口。</p>
 * <br/>
 * <p>深克隆与浅克隆的实现：</p>
 * <p>	1. 浅克隆：复制对象，当对象被复制时，只复制它本身和其中包含的值类型的成员变量，而引用类型的成员变量并没有复制。</p>
 * <p>	2. 深克隆：复制对象，当对象被复制时，除了它本身被复制外，其中包含的引用对象也将被复制，而不像浅克隆那样只复制它本身。</p>
 * <br/>
 * <p>深克隆和浅克隆的区别：</p>
 * <p>  1. 浅克隆：被复制的对象的所有变量都含有与原来的对象相同的值，而所有的对其他对象的引用仍然指向原来的对象。换言之，浅复制仅仅复制所考虑的对象，而不复制它所引用的对象。</p>
 * <p>  2. 深克隆：被复制的对象的所有变量都含有与原来的对象相同的值，除去那些引用其他对象的变量。那些指向其他对象的引用也复制了，指向的对象也将按照这些引用所指的对象进行复制，这样，就形成了原对象的一个完整副本。</p>
 * <br/>
 * <p>深克隆的优点：</p>
 * <p>  1. 深克隆相比于浅克隆速度较慢并且花销较大。</p>
 * <p>  2. 深克隆可以将对象复制到内存中不同的位置，而不需要在原始内存中保留被复制的对象。</p>
 * <p>  3. 深克隆可以实现真正意义上的“对象复制”，而浅克隆只是复制引用。</p>
 * <br/>
 * <p>深克隆的缺点：</br>
 * <p>  1. 实现深克隆需要开发人员花更多的时间。</p>
 * <p>  2. 需要完整地实现Cloneable接口。</p>
 * <br/>
 * <p>深克隆使用场景：</p>
 * <p>  1. 当你需要将一个对象序列化到本地文件或者数据库，然后从本地文件或数据库中恢复该对象的时候。</p>
 * <p>  2. 当程序中包含有循环引用问题的时候。</p>
 * <p>  3. 当在程序中创建的对象需要被共享的时候。</p>
 *
 */
@Log4j2
public class Client {
    public static void main(String[] args) throws CloneNotSupportedException, IOException, ClassNotFoundException {
        Citation citation = new Citation();
        Stutent stutent = new Stutent();
        stutent.setName("张三");
        citation.setStutent(stutent);
        //创建对象输出流对象
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("citation.txt"));
        //写对象
        oos.writeObject(citation);
        //关闭流
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("citation.txt"));
        Citation citation1 = (Citation) ois.readObject();
        ois.close();
        Stutent stutent1 = citation1.getStutent();
        stutent1.setName("李四");
        citation.show();
        citation1.show();
    }
}
