package com.design.creational.prototype.shallowcloning;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Client {
    public static void main(String[] args) throws CloneNotSupportedException {
        //创建一个原型对象
        Realizetype realizetype = new Realizetype();
        //调用Realizetype类中的clone方法进行对象的克隆
        Realizetype realizetype1 = realizetype.clone();
        log.info("realizetype1:{}", realizetype1);
        log.info("realizetype:{}", realizetype);
        log.info("realizetype1==realizetype:{}", realizetype1 == realizetype);
        log.info("realizetype1.equals(realizetype):{}", realizetype1.equals(realizetype));
    }
}
