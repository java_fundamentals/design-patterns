package com.design.creational.prototype.shallowcloning.example;

import lombok.extern.log4j.Log4j2;

/**
 * 奖状类
 */
@Log4j2
public class Citation implements Cloneable{
    private String name;

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void show(){
        log.info("{}同学：在2024学年第一学期中表现优秀，被评为三好学生。特发此状！",this.name);
    }

    @Override
    protected Citation clone() throws CloneNotSupportedException {
        return (Citation) super.clone();
    }
}
