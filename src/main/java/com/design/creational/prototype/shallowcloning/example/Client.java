package com.design.creational.prototype.shallowcloning.example;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Client {
    public static void main(String[] args) throws CloneNotSupportedException {
        Citation citation = new Citation();
        citation.setName("张三");
        citation.show();
        Citation citation1 = citation.clone();
        citation1.setName("李四");
        citation1.show();
        Citation citation2 = citation.clone();
        citation2.setName("王五");
        citation2.show();
    }
}
