package com.design.creational.prototype.shallowcloning;

import lombok.extern.log4j.Log4j2;

/**
 * 原型模式
 * 定义：
 *    创建型模式，使用原型实例指定要创建的对象类型，并且通过复制这个原型来创建新的对象。
 * 角色：
 *    1、抽象原型类：声明克隆方法；
 *    2、具体原型类：实现克隆方法，创建克隆对象；
 *
 * 适用场景：
 *    1、资源优化场景；
 *    2、类初始化需要消化非常多的资源，这个资源包括数据、硬件资源等；
 *    3、性能和安全要求的场景，比如需要通过远程方法创建一个对象，并且通过远程方法传递，可能需要消耗较长的时间。
 *    4、通过new产生一个对象需要非常繁琐的数据准备或访问权限，则可以使用原型模式；
 *    5、一个对象多个修改者的场景；
 *    6、一个对象需要提供给其他对象访问，而且各个调用者可能都需要修改其值时，可以考虑使用原型模式拷贝一个对象给调用者。
 *
 * 实现方式：
 *    1、实现Cloneable接口；
 *    2、重写Object类中的clone方法；
 *
 * 优点：
 *    1、性能提高；
 *    2、逃避构造函数的约束；
 *    3、逃避单例模式的限制。
 * 缺点：
 *    1、配备克隆方法需要对类的功能进行通盘考虑，这对于全新的类不是很难，但对于已有的类不一定很容易
 *    2、必须实现Cloneable接口；
 * @author liuc
 */
@Log4j2
public class Realizetype implements Cloneable {
    public Realizetype() {
        log.info("具体的原型对象创建完成");
    }
    /**
     * 浅克隆
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    protected Realizetype clone() throws CloneNotSupportedException {
        log.info("具体原型复制成功");
        return (Realizetype) super.clone();
    }
}
