package com.design.creational.builder;

/**
 * 指挥者类
 */
public class Director {

    // 持有当前需要使用的构建器对象
    private Builder builder;

    // 构造器，传入构建器对象
    public Director(Builder builder) {
        this.builder = builder;
    }

    // 产品构建与组装方法
    public Bike construct() {
        builder.buildFrame();
        builder.buildSeat();
        return builder.createBike();
    }
}
