package com.design.creational.builder;

import lombok.extern.log4j.Log4j2;

/**
 * 建造者模式
 * 定义：
 * 将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示。
 * 建造者模式是一步一步创建一个复杂的对象，它允许用户只通过指定复杂对象的类型和内容就可以构建它们，用户不需要知道
 * 内部的具体构建细节。
 *
 * 建造者模式的主要优点有：
 * 1、建造者独立，易扩展。
 * 2、便于控制细节风险。
 *
 * 建造者模式的主要缺点是：
 * 1、产品必须有共同点，范围定义比较明确。
 * 2、如内部变化复杂，会有很多的建造类。
 *
 * 使用场景：
 * 1、需要生成的对象具有复杂的内部结构。
 * 2、需要生成的对象内部属性本身相互依赖。
 *
 * 注意事项：与工厂模式的区别是：建造者模式更加关注与零件装配的顺序。
 *
 */
@Log4j2
public class Client {
    public static void main(String[] args) {

        // 创建一个Director对象
        Director director = new Director(new MobileBuilder());
        //让指挥者指挥组装自行车
        Bike bike = director.construct();
        log.info(bike.getFrame());
        log.info(bike.getSeat());
    }
}
