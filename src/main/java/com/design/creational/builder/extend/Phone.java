package com.design.creational.builder.extend;

/**
 * 手机类
 */
public class Phone {
    // 手机的各个部分
    private String cpu;
    private String screen;
    private String memory;
    private String mainboard;

    // 私有构造函数
    private Phone(Builder builder) {
        this.cpu = builder.cpu;
        this.screen = builder.screen;
        this.memory = builder.memory;
        this.mainboard = builder.mainboard;
    }

    // 静态内部类
    public static class Builder {

        // 手机的各个部分
        private String cpu;
        private String screen;
        private String memory;
        private String mainboard;

        // 必须的参数
        public Builder cpu(String cpu) {
            this.cpu = cpu;
            return this;
        }

        // 必须的参数
        public Builder screen(String screen) {
            this.screen = screen;
            return this;
        }

        public Builder memory(String memory) {
            this.memory = memory;
            return this;
        }

        public Builder mainboard(String mainboard) {
            this.mainboard = mainboard;
            return this;
        }

        // 返回对象
        public Phone build() {
            return new Phone(this);
        }
    }

    @Override
    public String toString() {
        return "Phone{" +
                "cpu='" + cpu + '\'' +
                ", screen='" + screen + '\'' +
                ", memory='" + memory + '\'' +
                ", mainboard='" + mainboard + '\'' +
                '}';
    }
}
