package com.design.creational.builder;

import lombok.extern.log4j.Log4j2;

/**
 * 具体的构建者，用来构建摩拜单车对象
 */
@Log4j2
public class MobileBuilder extends Builder{

    public void buildFrame() {
        bike.setFrame("碳纤维车架");
    }

    public void buildSeat() {
        bike.setSeat("真皮车座");
    }

    @Override
    public Bike createBike() {
        return bike;
    }

}
