package com.design.creational.builder;

public abstract class Builder {
    //申明Bike类型变量，并进行赋值
    protected Bike bike = new Bike();

    //构建自行车框架
    public abstract void buildFrame();

    //构建自行车座
    public abstract void buildSeat();

    //建造自行车
    public abstract Bike createBike();
}
