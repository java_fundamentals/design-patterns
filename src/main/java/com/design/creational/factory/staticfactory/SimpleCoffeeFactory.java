package com.design.creational.factory.staticfactory;

/**
 * 简单咖啡工厂类，用于生产咖啡
 */
public class SimpleCoffeeFactory {

    /**
     *  静态工厂方法，根据传入的类型返回对应的咖啡对象
     * @param type
     * @return
     */
    public static Coffee createCoffee(String type) {
        if ("american".equals(type)) {
            return new AmericanCoffee();
        } else if ("latte".equals(type)) {
            return new LatteCoffee();
        } else {
            throw new RuntimeException("没有此咖啡");
        }
    }
}
