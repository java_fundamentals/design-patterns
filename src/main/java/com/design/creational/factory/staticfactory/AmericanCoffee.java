package com.design.creational.factory.staticfactory;

import lombok.extern.log4j.Log4j2;

/**
 * 美式咖啡
 */
@Log4j2
public class AmericanCoffee extends Coffee {
    @Override
    public String getName() {
        return "美式咖啡";
    }
}

