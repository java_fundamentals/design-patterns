package com.design.creational.factory.staticfactory;


/**
 * 咖啡店
 */
public class CoffeeStore {
    /**
     * 点咖啡
     * @param type
     * @return
     */
    public Coffee orderCoffee(String type) {
        // 点咖啡
        Coffee coffee = SimpleCoffeeFactory.createCoffee(type);
        coffee.addMilk();
        coffee.addSugar();
        return coffee;
    }
}
