package com.design.creational.factory.staticfactory;

/**
 * 拿铁咖啡
 */
public class LatteCoffee extends Coffee {

    @Override
    public String getName() {
        return "拿铁咖啡";
    }
}
