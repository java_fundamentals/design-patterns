package com.design.creational.factory.config_factory;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Client {
    public static void main(String[] args) {
       Coffee coffee = CoffeeFactory.createCoffee("american");
       log.info(coffee.getName());
    }
}
