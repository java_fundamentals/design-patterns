package com.design.creational.factory.config_factory;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class CoffeeFactory {
    /**
     * 加载配置文件，获取配置文件中配置的全类名，并创建该累的对象进行存储
     * key：配置文件中配置的类名
     * value：创建的该类对象
     */
    private static HashMap<String,Coffee> map = new HashMap<>();

    /**
     * 加载配置文件
     */
    static {
        Properties p = new  Properties();
        //加载配置文件
        try {
            p.load(CoffeeFactory.class.getClassLoader().getResourceAsStream("bean.properties"));
            //遍历配置文件
            for (Object o : p.keySet()) {
                String key = o.toString();
                String value = p.getProperty(key);
                //通过反射创建对象
                Class<?> clazz = Class.forName(value);
                //创建对象
                Coffee coffee = (Coffee) clazz.newInstance();
                //存储进map中
                map.put(key, coffee);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *  静态代码块，在类加载的时候执行一次
     *  根据配置文件或输入参数来决定实例化哪个子类
     *  比如，根据配置文件中设置的Coffee类型来实例化对应的Coffee子类
     *  如果输入参数为 latte，则返回LatteCoffee实例
     *  如果输入参数为americano，则返回AmericanCoffee实例
     * @param name
     * @return
     */
    public static Coffee createCoffee(String name){
        return map.get(name);
    }
}
