package com.design.creational.factory.before;

/**
 * 咖啡店
 */
public class CoffeeStore {
    /**
     * 点咖啡
     * @param type
     * @return
     */
    public Coffee orderCoffee(String type) {
        Coffee coffee = null;
        if ("ameraican".equalsIgnoreCase(type)) {
            coffee = new AmericanCoffee();
        } else if ("latte".equalsIgnoreCase(type)) {
            coffee = new LatteCoffee();
        } else {
            throw new RuntimeException("没有此咖啡");
        }
        coffee.addMilk();
        coffee.addSugar();
        return coffee;
    }
}
