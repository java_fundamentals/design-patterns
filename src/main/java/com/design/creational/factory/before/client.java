package com.design.creational.factory.before;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class client {
    public static void main(String[] args) {
        CoffeeStore store = new CoffeeStore();
        Coffee coffee = store.orderCoffee("latte");
        //输出 latte
        log.info(coffee.getName());
    }
}
