package com.design.creational.factory.abstractfactory;

/**
 * 甜点抽象类
 */
public abstract class Dessert {
    // 抽象方法，显示甜点信息
    public abstract void show();
}
