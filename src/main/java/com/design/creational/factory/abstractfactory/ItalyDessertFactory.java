package com.design.creational.factory.abstractfactory;

/**
 *  意大利风味甜品工厂
 *  可以生产意大利风味的甜点和拿铁咖啡
 */
public class ItalyDessertFactory implements DessertFactory {
    // 生产拿铁咖啡 生产拿铁咖啡
    @Override
    public Coffee createCoffee() {
        return new LatteCoffee();
    }

    // 生产意大利风味的甜点
    @Override
    public Dessert createDessert() {
        return new Tiramisu();
    }
}
