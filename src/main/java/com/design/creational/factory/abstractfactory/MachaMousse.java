package com.design.creational.factory.abstractfactory;

import lombok.extern.log4j.Log4j2;

/**
 * 抹茶慕斯类
 */
@Log4j2
public class MachaMousse extends Dessert{
    // 实现代码
    @Override
    public void show() {
        log.info("抹茶慕斯");
    }
}
