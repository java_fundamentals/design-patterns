package com.design.creational.factory.abstractfactory;

/**
 * 美式风味甜品工厂
 * 可以生成美式咖啡和抹茶慕斯
 */
public class AmericanDessertFactory implements DessertFactory{
    //生产美式咖啡
    @Override
    public Coffee createCoffee() {
        return new AmericanCoffee();
    }

    //生产抹茶慕斯
    @Override
    public Dessert createDessert() {
        return new MachaMousse();
    }
}
