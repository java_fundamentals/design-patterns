package com.design.creational.factory.abstractfactory;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Client {
    public static void main(String[] args) {
        //创建的是意大利风味甜品工厂对象
        ItalyDessertFactory factory = new ItalyDessertFactory();
        //通过工厂对象创建不同口味的甜品对象
        Coffee coffee = factory.createCoffee();
        Dessert dessert = factory.createDessert();
        log.info("客户点了意大利口味的咖啡和甜品");
        log.info(coffee.getName());
        dessert.show();

    }
}
