package com.design.creational.factory.abstractfactory;

import lombok.extern.log4j.Log4j2;

/**
 * 提拉米苏类
 */
@Log4j2
public class Tiramisu extends Dessert{
    @Override
    public void show() {
        log.info("提拉米苏");
    }
}
