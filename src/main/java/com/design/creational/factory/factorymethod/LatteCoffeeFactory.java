package com.design.creational.factory.factorymethod;

/**
 *  拿铁咖啡工厂，专门用于生产拿铁咖啡
 */
public class LatteCoffeeFactory implements CoffeeFactory{
    @Override
    public Coffee createCoffee() {
        return new LatteCoffee();
    }
}
