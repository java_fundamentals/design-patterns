package com.design.creational.factory.factorymethod;

/**
 *  美式咖啡工厂，专门用来生产美式咖啡
 */
public class AmericanCoffeeFactory implements CoffeeFactory {
    /**
     *  创建咖啡对象
     * @return
     */
    @Override
    public Coffee createCoffee() {
        return new AmericanCoffee();
    }
}
