package com.design.creational.factory.factorymethod;

import lombok.extern.log4j.Log4j2;

/**
 * 咖啡类
 */
@Log4j2
public abstract class Coffee {
    public abstract String getName();

    /**
     * 加糖
     */
    public void addSugar(){
        log.info("加糖");
    }

    public void addMilk(){
        log.info("加奶");
    }
}
