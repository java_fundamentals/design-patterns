package com.design.creational.factory.factorymethod;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Client {
    public static void main(String[] args) {
        //创建咖啡店对象
        CoffeeStore coffeeStore = new CoffeeStore();
        //订购美式咖啡
        CoffeeFactory factory = new AmericanCoffeeFactory();
        coffeeStore.setFactory(factory);
        Coffee coffee = coffeeStore.orderCoffee();
        log.info(coffee.getName());
    }
}
