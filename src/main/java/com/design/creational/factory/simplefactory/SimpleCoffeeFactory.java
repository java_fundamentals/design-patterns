package com.design.creational.factory.simplefactory;

/**
 * 简单咖啡工厂类，用于生产咖啡
 */
public class SimpleCoffeeFactory {

    //简单工厂方法
    public Coffee createCoffee(String type) {
        if ("american".equals(type)) {
            return new AmericanCoffee();
        } else if ("latte".equals(type)) {
            return new LatteCoffee();
        } else {
            throw new RuntimeException("没有此咖啡");
        }
    }
}
