package com.design.creational.factory.simplefactory;


/**
 * 咖啡店
 */
public class CoffeeStore {
    /**
     * 点咖啡
     * @param type
     * @return
     */
    public Coffee orderCoffee(String type) {
        // 创建咖啡店类
        SimpleCoffeeFactory factory = new SimpleCoffeeFactory();
        // 点咖啡
        Coffee coffee = factory.createCoffee(type);
        coffee.addMilk();
        coffee.addSugar();
        return coffee;
    }
}
