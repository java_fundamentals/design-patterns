package com.design.creational.factory.simplefactory;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Client {
    public static void main(String[] args) {
        // 使用简单工厂模式创建对象
        CoffeeStore store = new CoffeeStore();
        Coffee coffee = store.orderCoffee("latte");
        log.info(coffee.getName());
    }
}
