package com.design.behavioral.state;

import lombok.extern.log4j.Log4j2;

/**
 * 电梯关闭状态类
 */
@Log4j2
public class ClosingState extends LiftState{
    //电梯门关闭，这是关闭状态要实现的动作
    @Override
    public void close() {
        log.info("电梯门关闭...");
    }

    // 电梯门关了再打开，逗你玩呢，那这个允许呀
    @Override
    public void open() {
        super.context.setLiftState(Context.OPENING_STATE);
        super.context.open();
    }

    //电梯门关了就跑，这是在正常不过的了
    @Override
    public void run() {
        super.context.setLiftState(Context.RUNNING_STATE);
        super.context.run();
    }

    // 电梯门关着，我就不按楼层
    @Override
    public void stop() {
        super.context.setLiftState(Context.STOPPING_STATE);
        super.context.stop();
    }
}
