package com.design.behavioral.state;

import lombok.extern.log4j.Log4j2;

/**
 * 电梯停止状态类
 */
@Log4j2
public class StoppingState extends LiftState{
    // 虽然可以开门，但这个动作不归我执行
    @Override
    public void close() {
        // 状态修改
        context.setLiftState(Context.CLOSING_STATE);
        //动作委托为CloseState来执行,也就是委托给了ClosingState子类执行这个动作
        super.context.getLiftState().close();
    }

    // 停止状态，开门，那是要的
    @Override
    public void open() {
        // 状态修改
        context.setLiftState(Context.OPENING_STATE);
        //动作委托为CloseState来执行,也就是委托给了ClosingState子类执行这个动作
        super.context.getLiftState().open();
    }

    //停止状态在跑起来，正常的很
    @Override
    public void run() {
        // 状态修改
        context.setLiftState(Context.RUNNING_STATE);
        //动作委托为CloseState来执行,也就是委托给了ClosingState子类执行这个动作
        super.context.getLiftState().run();
    }

    //停止状态是怎么发生的呢？当然是停止方法执行的了
    @Override
    public void stop() {

    }
}
