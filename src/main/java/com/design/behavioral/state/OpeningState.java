package com.design.behavioral.state;

import lombok.extern.log4j.Log4j2;

/**
 * 电梯开启状态类
 */
@Log4j2
public class OpeningState extends LiftState{
    @Override
    public void close() {
        //修改状态
        super.context.setLiftState(Context.CLOSING_STATE);
        //调用当前状态中的context中的close方法
        super.context.close();
    }

    @Override
    public void open() {
        log.info("电梯开启...");
    }

    @Override
    public void run() {
        //什么都不做
    }

    @Override
    public void stop() {
        //什么都不做
    }
}
