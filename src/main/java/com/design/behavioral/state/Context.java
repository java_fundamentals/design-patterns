package com.design.behavioral.state;

/**
 * 环境角色类
 */
public class Context {
    //定义对应状态对象的常量
    public static final OpeningState OPENING_STATE = new OpeningState();
    public static final ClosingState CLOSING_STATE = new ClosingState();
    public static final RunningState RUNNING_STATE = new RunningState();
    public static final StoppingState STOPPING_STATE = new StoppingState();

    //当前状态
    private LiftState state;

    public LiftState getLiftState() {
        return state;
    }
    //设置当前状态对象
    public void setLiftState(LiftState state) {
        this.state = state;
        this.state.setContext(this);
    }

    public void open() {
        state.open();
    }
    public void close() {
        state.close();
    }
    public void run() {
        state.run();
    }
    public void stop() {
        state.stop();
    }
}
