package com.design.behavioral.state;

import lombok.extern.log4j.Log4j2;

/**
 * 电梯运行状态类
 */
@Log4j2
public class RunningState extends LiftState{
    // 电梯门关闭？这是肯定了
    @Override
    public void close() { //虽然可以关闭，但是这个动作不归我执行

    }

    // 运行的时候开电梯门？你疯啦！电梯不会给你开的
    @Override
    public void open() {

    }

    //这是在运行状态下要实现的方法
    @Override
    public void run() {
        log.info("电梯正在运行...");
    }

    // 这个事绝对是合理的，光运行不停止还有谁敢坐这个电梯？！
    @Override
    public void stop() {
        super.context.setLiftState(Context.STOPPING_STATE);
        super.context.stop();
        log.info("电梯停止了...");
    }
}
