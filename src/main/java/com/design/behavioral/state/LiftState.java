package com.design.behavioral.state;

/**
 * 抽象状态类
 */
public abstract class LiftState {
    //申明环境角色变量
    Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    //电梯门关闭
    public abstract void close();

    //电梯门开启
    public abstract void open();

    //电梯跑起来
    public abstract void run();

    //电梯停止
    public abstract void stop();
}
