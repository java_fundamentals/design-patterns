package com.design.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 具体主题角色类
 */
public class SubscriptionSubject implements Subject{
    //定义一个集合，用来存储多个观察者对象
    private List<Observer> observers = new ArrayList<>();
    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers(String msg) {
        //遍历观察者集合，调用每一个观察者的update方法
        for (Observer observer : observers) {
            observer.update(msg);
        }
    }
}
