package com.design.behavioral.observer;

/**
 * 抽象主题角色类
 */
public interface Subject {

    // 注册观察者
    void registerObserver(Observer o);

    // 移除观察者
    void removeObserver(Observer o);

    // 通知观察者
    void notifyObservers(String msg);
}
