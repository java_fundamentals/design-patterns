package com.design.behavioral.observer;

/**
 * 抽象观察者类
 */
public interface Observer {

    /**
     * 更新方法
     */
    void update(String msg);
}
