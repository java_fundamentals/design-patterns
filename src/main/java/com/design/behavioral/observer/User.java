package com.design.behavioral.observer;

import lombok.extern.log4j.Log4j2;

/**
 * 具体的观察者角色类
 */
@Log4j2
public class User implements Observer{
    private String name;

    public User(String name) {
        this.name = name;
    }

    @Override
    public void update(String msg) {
        log.info("{}收到消息：{}", name, msg);
    }
}
