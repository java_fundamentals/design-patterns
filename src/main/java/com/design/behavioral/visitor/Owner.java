package com.design.behavioral.visitor;

import lombok.extern.log4j.Log4j2;

/**
 * 具体访问者角色类（自己）
 */
@Log4j2
public class Owner implements Person{
    @Override
    public void feed(Cat cat) {
        log.info("主人喂猫");
    }

    @Override
    public void feed(Dog dog) {
        log.info("主人喂狗");
    }
}
