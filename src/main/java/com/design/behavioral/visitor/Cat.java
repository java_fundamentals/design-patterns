package com.design.behavioral.visitor;

import lombok.extern.log4j.Log4j2;

/**
 * 其他元素角色类（宠物猫）
 */
@Log4j2
public class Cat implements Animal{
    @Override
    public void accept(Person person) {
        log.info("{}正在访问: {}", person.getClass().getSimpleName(), this.getClass().getSimpleName());
        // 调用访问者访问的方法
        person.feed(this);
    }
}
