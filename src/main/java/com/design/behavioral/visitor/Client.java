package com.design.behavioral.visitor;

import lombok.extern.log4j.Log4j2;

/**
 * 访问者模式
 * 定义：
 *      访问者模式表示一个作用于某对象结构中的各元素的操作。它使你可以在不改变各元素的类的前提下定义作用于这些元素的新操作
 * 优点：
 *      1、符合单一职责原则，访问者模式将有关元素对象的职责与其自身操作的职责分开。
 *      2、增加新的操作很容易，无须修改原有代码，符合开闭原则。
 * 缺点：
 *      1、增加新的元素类很困难。在访问者模式中，每增加一个新的元素类，都要在每一个具体访问者类中添加相应的具体操作，这违背了"开闭原则"
 *      2、破坏封装。访问者模式中具体元素对访问者公布细节，这破坏了对象的封装性。
 * 使用场景：
 *      1、对象结构中对象对应的类很少改变，但经常需要在此对象结构上定义新的操作。
 *      2、需要对一个对象结构中的对象进行很多不同的并且不相关的操作，而避免让这些操作"污染"这些对象的类。
 *      3、访问者模式适用于数据结构相对稳定的系统。
 * 注意事项：
 *      1、具体元素对访问者公布细节，违反了迪米特原则。
 *      2、具体元素变更比较困难。
 *      3、具体元素高度集中，导致系统维护困难。
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        //创建Home对象
        Home home = new Home();
        home.add(new Cat());
        home.add(new Dog());
        //创建一个具体访问者对象
        Owner owner = new Owner();
        //调用访问者对象访问元素对象
        home.action(owner);

        SomeOne someOne = new SomeOne();
        home.action(someOne);
    }
}
