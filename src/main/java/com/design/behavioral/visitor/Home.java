package com.design.behavioral.visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * 对象结构类（家）
 */
public class Home {
    //申明一个集合对象，用来存储元素对象
    private List<Animal> animals = new ArrayList<>();


    //对象结构类提供给外部访问的方法，用来给对象结构类添加元素对象
    public void add(Animal animal) {
        animals.add(animal);
    }

    //对象结构类提供给外部访问的方法，用来遍历存储元素对象的集合，调用元素对象的方法，让元素对象访问自身
    public void action(Person person) {
        for (Animal animal : animals) {
            animal.accept(person);
        }
    }
}
