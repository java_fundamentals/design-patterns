package com.design.behavioral.visitor;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Dog implements Animal{
    @Override
    public void accept(Person person) {
        log.info("{}正在访问: {}", person.getClass().getSimpleName(), this.getClass().getSimpleName());
        // 调用访问者的访问方法
        person.feed(this);
    }
}
