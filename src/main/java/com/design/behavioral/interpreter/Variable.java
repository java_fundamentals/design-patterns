package com.design.behavioral.interpreter;

import lombok.extern.log4j.Log4j2;

/**
 * 封装变量的类
 */
@Log4j2
public class Variable extends AbstractExpression{
    // 申明变量名的成员变量
    private String variableName;

    public Variable(String variableName) {
        this.variableName = variableName;
    }

    public String getVariableName() {
        return variableName;
    }

    /**
     * 解释变量，获取变量的值
     * @param context
     */
    @Override
    public int interpret(Context context) {
        return context.getValue(this);
    }


    @Override
    public String toString() {
        return variableName;
    }
}
