package com.design.behavioral.interpreter;

/**
 * 抽象表达式类
 * 解释器模式的抽象表达式类，定义解释器的接口。
 */
public abstract class AbstractExpression {
    public abstract int interpret(Context context);
}
