package com.design.behavioral.interpreter;

import lombok.extern.log4j.Log4j2;

/**
 * 解释器模式
 * 定义：
 *       给定一个语言，定义它的文法表示，并定义一个解释器，这个解释器使用该表示来解释语言中的句子。
 * 解释器模式优点：
 *       易于改变和扩展文法。
 *       实现文法较为容易。
 *       增加了新的解释表达式的方式。
 * 解释器模式缺点：
 *       可能会产生过多的类，增加系统复杂度。
 *       解释器模式会引起类膨胀。
 * 适用场景：
 *       对于一些固定文法的语言，可以构建一个解释器，该解释器可以解释该语言中的句子。
 *       对于一些简单文法的语言，可以直接使用解释器模式。
 *       对于复杂的语言，可以构建解释器模式来解释该语言。
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        //创建环境变量
        Context context = new Context();
        //创建多个变量对象
        Variable a = new Variable("a");
        Variable b = new Variable("b");
        Variable c = new Variable("c");
        Variable d = new Variable("d");
        //将变量对象添加到环境变量中
        context.assign(a, 1);
        context.assign(b, 2);
        context.assign(c, 3);
        context.assign(d, 4);

        //创建表达式对象 (a - ((b - c) + d))
        AbstractExpression expression = new Minus(a, new Plus(new Minus(b, c),d));
        log.info("表达式{}计算结果为：{}",expression,expression.interpret(context));
    }
}
