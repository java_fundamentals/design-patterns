package com.design.behavioral.interpreter;

import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

/**
 * 环境角色类
 */
@Log4j2
public class Context {
    //定义一个map来存放变量和值的对应关系
    private Map<Variable, Integer> map = new HashMap<>();

    //设置变量的值
    public void assign(Variable variable, int value) {
        map.put(variable, value);
    }

    //获取变量的值
    public int getValue(Variable variable) {
        return map.get(variable);
    }

    //清空变量
    public void clear() {
        map.clear();
    }

    //打印变量和值的对应关系
    public void print() {
        for (Map.Entry<Variable, Integer> entry : map.entrySet()) {
            log.info(entry.getKey() + " = " + entry.getValue());
        }
    }

}
