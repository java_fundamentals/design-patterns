package com.design.behavioral.mediator;

import lombok.extern.log4j.Log4j2;

/**
 * 具体的同事角色类
 */
@Log4j2
public class HouseOwner extends Person{
    public HouseOwner(String name, Mediator mediator) {
        super(name, mediator);
    }

    // 发送消息
    public void contact(String message) {
        mediator.contact(message, this);
    }

    /**
     * 获取信息
     * @param message
     */
    public void getMessage(String message) {
        log.info("房主" + name + "收到消息是：" + message);
    }
}
