package com.design.behavioral.mediator;

import lombok.extern.log4j.Log4j2;

/**
 * 具体的同事角色类
 */
@Log4j2
public class Tenant extends Person{
    public Tenant(String name, Mediator mediator) {
        super(name, mediator);
    }

    /**
     * 与中介者联系
     * @param message
     */
    public void contact(String message) {
        mediator.contact(message, this);
    }

    /**
     * 向中介者发送消息
     * @param message
     */
    public void getMessage(String message) {
        log.info("租房者{}收到消息是：{}",name, message);
    }
}
