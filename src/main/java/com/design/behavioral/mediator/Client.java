package com.design.behavioral.mediator;

import lombok.extern.log4j.Log4j2;

/**
 * 中介者模式
 * 定义：
 *      一个中介者对象封装一系列对象交互，中介者使各对象不需要显式地相互引用，从而使其耦合松散，而且可以独立地改变它们之间的交互。
 * 类型：
 *      行为型
 * 实现方式：
 *      1. 创建一个 Mediator 接口
 *      2. 创建 ConcreteMediator 类
 *      3. 创建 Colleague 类
 *      4. 创建 Client 类
 * 角色：
 *      Mediator：定义一个接口用于与 Colleague 对象通信
 *      ConcreteMediator：实现 Mediator 接口，定义一个 List 来保存 Colleague 对象，协调各个 Colleague 对象之间的交互
 *      Colleague：抽象类，定义一个接口用于 Colleague 对象之间进行交互
 *      ConcreteColleague：继承 Colleague 类，实现具体交互行为
 *      Client：客户端类，创建 Colleague 对象和 Mediator 对象，并将它们关联起来
 * 适用场景：
 *      多个对象之间存在复杂的引用关系，导致它们之间的依赖关系结构混乱且难以复用该对象。
 * 优点：
 *      中介者模式简化了对象之间的交互，使得它们不需要显式地相互引用，从而使得耦合关系更加灵活，并易于维护
 *      将各同事对象之间的交互封装到一个中介者对象中，使得系统更容易扩展
 * 缺点：
 *      由于中介者封装了复杂的交互关系，因此当需要理解整个系统的交互时，可能需要一些时间
 *      中介者模式将各同事对象与中介者对象耦合，如果中介者对象变化的话，可能会影响到与之相互引用的同事对象
 *      中介者模式使得系统对象之间耦合性增加，使得对象之间的依赖关系变得复杂
 * 使用场景：
 *      多个对象之间存在复杂的交互关系，且这些对象之间相互引用。
 *      想通过一个中间类来封装多个类之间的交互，可以使用中介者模式。
 *      一个对象引用其他很多对象并且直接与这些对象通信，导致难以复用该对象时，可以考虑使用中介者模式。
 *      需要将对象间的关系进行解耦时，可以使用中介者模式，将对象间的一对多依赖关系转变成多对多依赖关系，从而减少耦合。
 *      需要让多个对象之间进行比较复杂的行为时，可以使用中介者模式，将它们之间的行为进行封装，以减少它们之间的依赖关系
 * 注意事项：
 *      中介者模式适用于多个对象之间存在复杂的交互关系且难以理清它们之间的依赖关系的情况
 *      在实际开发中，可以使用中介者模式来封装对象之间的交互关系，以简化系统的维护
 *      如果对象之间的交互关系复杂且难以维护，可以考虑使用中介者模式来重构系统
 *      在实现中介者模式时，需要确保中介者对象的设计合理，避免过度使用中介者模式导致系统复杂性增加
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        MediatorStructure mediator = new MediatorStructure();

        // 创建同事对象
        HouseOwner houseOwner = new HouseOwner("张三", mediator);
        Tenant tenant = new Tenant("李四", mediator);

        // 注册同事对象
        mediator.setHouseOwner(houseOwner);
        mediator.setTenant(tenant);

        // 发送消息
        tenant.contact("我要租三室一厅的房子!!!");
        houseOwner.contact("我这里有三室一厅的房子，你要租吗？");

    }
}
