package com.design.behavioral.command;

import lombok.extern.log4j.Log4j2;

import java.util.Map;

/**
 * 具体的命令类
 */
@Log4j2
public class OrderCommand implements Command{
    // 持有接收者对象
    private SeniorChef seniorChef;

    private Order order;

    public OrderCommand(SeniorChef seniorChef, Order order) {
        this.seniorChef = seniorChef;
        this.order = order;
    }

    @Override
    public void execute() {
        log.info("{}桌的订单：",order.getDiningTable());
        Map<String,Integer> foodDir = order.getFoodDir();
        //遍历map集合
        for(String foodName : foodDir.keySet()){
            seniorChef.makeFood(foodName, foodDir.get(foodName));
        }
        log.info("上菜");
    }
}
