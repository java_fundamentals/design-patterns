package com.design.behavioral.command;

import lombok.extern.log4j.Log4j2;

/**
 * 命令模式
 * 定义：
 *      将一个请求封装为一个对象，从而使你可用不同的请求对客户进行参数化；对请求排队或记录请求日志，以及支持可撤销的操作
 * 类型：
 *      行为型
 * 优点：
 *      1、降低了系统耦合度
 *      2、新的命令可以很容易地加入到系统中
 *      3、可以比较容易地设计一个命令队列和宏命令，使得系统可以支持事务操作
 *      4、可以方便地实现对请求的撤销和恢复
 * 缺点：
 *      使用命令模式可能会导致某些系统有过多的具体命令类
 *      系统使用命令模式，会导致产生大量具体命令类
 *      这些具体命令类往往都很简单，如果使用继承方式，会造成大量代码重复
 *      所以，命令模式最好适用于：需要将一组操作组合在一起，即支持宏命令的情况
 * 使用场景：
 *      1、系统需要将请求调用者和请求接收者解耦，使得调用者和接收者不直接交互
 *      2、系统需要在不同的时间指定请求、将请求排队和执行请求
 *      3、系统需要支持命令的撤销(Undo)操作和恢复(Redo)操作
 *      4、系统需要将一组操作组合在一起，即支持宏命令
 * 注意事项：
 *      1、通过调用Invoke()方法来执行命令
 *      2、通过调用Undo()方法来取消命令
 *      3、通过调用Redo()方法来重新执行被取消的命令
 *      4、通过调用SetCommand()方法来设置命令
 *      5、通过调用AddCommand()方法可以设置宏命令
 *      6、通过调用RemoveCommand()方法可以取消宏命令中的命令
 *      7、通过调用ClearCommand()方法可以清除宏命令中的所有命令
 *      8、通过调用ExecuteCommand()方法可以执行宏命令
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        // 创建第一个订单对象
        Order order1 = new Order();
        order1.setDiningTable(1);
        order1.setFood("鱼香肉丝", 1);
        order1.setFood("拍黄瓜", 2);
        order1.setFood("可乐", 3);

        // 创建第二个订单对象
        Order order2 = new Order();
        order2.setDiningTable(4);
        order2.setFood("宫保鸡丁", 1);
        order2.setFood("烤鸭", 1);
        order2.setFood("土豆丝", 1);
        order2.setFood("雪碧", 4);

        // 创建厨师对象
        SeniorChef seniorChef = new SeniorChef();
        // 创建命令对象，并设置订单
        OrderCommand orderCommand1 = new OrderCommand(seniorChef, order1);
        OrderCommand orderCommand2 = new OrderCommand(seniorChef, order2);

        // 创建服务员对象
        Waiter waiter = new Waiter();

        // 服务员接受订单
        waiter.setCommand(orderCommand1);
        waiter.setCommand(orderCommand2);
        // 执行命令
        waiter.orderUp();
    }
}
