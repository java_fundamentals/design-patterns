package com.design.behavioral.command;

import lombok.extern.log4j.Log4j2;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务员类（属于请求者角色）
 */
@Log4j2
public class Waiter {
    // 持有多个命令对象
    private List<Command> commands = new ArrayList<>();

    public void setCommand(Command command){
        // 添加命令对象
        commands.add(command);
    }

    // 发起命令功能
    public void orderUp(){
        log.info("服务员叫来厨师，菜单如下：");
        // 遍历命令对象集合
        for(Command command:commands){
            if (command != null) {
                // 执行命令
                command.execute();
            }
        }
    }
}
