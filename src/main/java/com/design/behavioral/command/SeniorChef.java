package com.design.behavioral.command;

import lombok.extern.log4j.Log4j2;

/**
 * 厨师类
 */
@Log4j2
public class SeniorChef {
    public void makeFood(String name,int num) {
        log.info("厨师正在制作" + name + "，数量为：" + num);
    }
}
