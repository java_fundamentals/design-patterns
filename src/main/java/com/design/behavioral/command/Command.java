package com.design.behavioral.command;

/**
 * 抽象命令类
 */
public interface Command {
    void execute();
}
