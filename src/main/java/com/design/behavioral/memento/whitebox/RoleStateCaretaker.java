package com.design.behavioral.memento.whitebox;

/**
 * 备忘录对象管理类
 */
public class RoleStateCaretaker {
    //申明RoleStateMemento对象
    private RoleStateMemento memento;

    public RoleStateMemento getMemento() {
        return memento;
    }
    public void setMemento(RoleStateMemento memento) {
        this.memento = memento;
    }
}
