package com.design.behavioral.memento.whitebox;

import lombok.extern.log4j.Log4j2;

/**
 * 备忘录模式
 * 定义：
 *      1. 客户端代码与备忘录对象之间没有直接的依赖关系，而是通过一个接口来间接的访问备忘录对象。
 *      2. 客户端代码通过接口来获取备忘录对象，并通过该备忘录对象来恢复对象状态。
 * 优点：
 *      1. 简化了客户端代码，降低了耦合度。
 *      2. 实现了信息隐藏，封装性较好。
 * 缺点：
 *      1. 增加了系统复杂度，需要维护一个备忘录对象。
 *      2. 备忘录对象需要实现一个接口，增加了系统的依赖。
 *      3. 客户端代码需要知道备忘录对象的具体实现类，增加了系统的耦合度。
 *      4. 备忘录对象需要存储对象的状态，占用内存资源。
 * 适用场景：
 *      1. 备忘录模式适用于需要保存和恢复对象状态的场景，如游戏存档、多级 undo/redo、数据恢复等。
 *      2. 备忘录模式可以帮助实现复杂系统的状态恢复，避免了使用大量的 if...else 语句。
 *      3. 备忘录模式可以帮助实现信息隐藏，封装性较好。
 *      4. 备忘录模式可以帮助实现撤销操作，提供一个可逆的操作。
 *      5. 备忘录模式可以帮助实现快照功能，提供一个可重复的操作。
 *      6. 备忘录模式可以帮助实现分布式事务，提供一个可靠的操作。
 *      7. 备忘录模式可以帮助实现游戏存档，提供一个可恢复的操作。
 *      8. 备忘录模式可以帮助实现多级 undo/redo，提供一个可回滚的操作。
 *      9. 备忘录模式可以帮助实现数据恢复，提供一个可恢复的操作。
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        log.info("大战Boss前...");
        GameRole gameRole = new GameRole();
        gameRole.initState();
        gameRole.showState();

        // 保存状态
        RoleStateCaretaker roleStateCaretaker = new RoleStateCaretaker();
        roleStateCaretaker.setMemento(gameRole.saveState());

        // 开始战斗
        log.info("大战Boss后...");
        gameRole.fight();
        gameRole.showState();

        log.info("恢复状态...");
        // 恢复状态
        gameRole.restoreState(roleStateCaretaker.getMemento());
        gameRole.showState();
    }
}
