package com.design.behavioral.memento.blackbox;

import lombok.extern.log4j.Log4j2;

/**
 * 游戏角色类（属于发起人角色）
 */
@Log4j2
public class GameRole {
    /**
     * 角色的生命值
     */
    private int vitality;
    /**
     * 角色的攻击力
     */
    private int attack;
    /**
     * 角色的防御力
     */
    private int defense;

    public GameRole() {

    }

    public GameRole(int vitality, int attack, int defense) {
        this.vitality = vitality;
        this.attack = attack;
        this.defense = defense;
    }

    /**
     * 初始化角色的状态（属于备忘录角色）
     * @return
     */
    public void initState(){
        this.vitality = 100;
        this.attack = 100;
        this.defense = 100;
    }

    /**
     * 保存角色的状态（属于备忘录角色）
     * @return
     */
    public Memento saveState(){
        return new RoleStateMemento(this.vitality, this.attack, this.defense);
    }

    /**
     * 恢复角色的状态（属于恢复者角色）
     * @param memento
     */
    public void restoreState(Memento memento){
        RoleStateMemento roleStateMemento = (RoleStateMemento) memento;
        this.vitality = roleStateMemento.getVitality();
        this.attack = roleStateMemento.getAttack();
        this.defense = roleStateMemento.getDefense();
    }

    /**
     * 展示状态功能
     */
    public void showState(){
        log.info("角色的生命值：{}" , this.vitality);
        log.info("角色的攻击力：{}" , this.attack);
        log.info("角色的防御力：{}" , this.defense);
    }

    /**
     * 战斗
     * @return
     */
    public void fight(){
        this.vitality = 0;
        this.attack = 0;
        this.defense = 0;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    private class RoleStateMemento implements Memento{
        private int vitality;
        private int attack;
        private int defense;
        public RoleStateMemento() {

        }

        public RoleStateMemento(int vitality, int attack, int defense) {
            this.vitality = vitality;
            this.attack = attack;
            this.defense = defense;
        }

        public int getVitality() {
            return vitality;
        }
        public void setVitality(int vitality) {
            this.vitality = vitality;
        }

        public int getAttack() {
            return attack;
        }

        public void setAttack(int attack) {
            this.attack = attack;
        }

        public int getDefense() {
            return defense;
        }

        public void setDefense(int defense) {
            this.defense = defense;
        }
    }
}
