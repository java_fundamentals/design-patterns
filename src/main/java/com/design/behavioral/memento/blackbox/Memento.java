package com.design.behavioral.memento.blackbox;

/**
 * 备忘录接口，对外提供窄接口，内部实现细节不暴露
 */
public interface Memento {

}
