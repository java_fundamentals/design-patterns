package com.design.behavioral.memento.blackbox;

import lombok.extern.log4j.Log4j2;

/**
 * 黑箱备忘录模式
 * 定义：
 *      在不暴露对象的内部状态的情况下，捕获并保存其内部状态，以便以后恢复。
 * 角色：
 *      GameRole：游戏角色，具有攻击力、生命值等属性，可以攻击、受伤、死亡等状态。
 *      RoleStateCaretaker：角色状态Caretaker，保存角色状态，并提供恢复角色状态的功能。
 *      Memento：备忘录接口，定义了保存和恢复角色状态的接口。
 *      ConcreteMemento：备忘录实体，实现Memento接口，保存角色的状态。
 * 协作：
 *      GameRole和RoleStateCaretaker之间通过Memento协议进行通信。
 * 适用场景：
 *      1. 需要保存和恢复对象内部状态的场景。
 *      2. 需要提供一个可回滚的操作的场景。
 * 类图：
 *      GameRole：保存角色状态，提供保存和恢复状态的功能。
 *      RoleStateCaretaker：保存角色状态，提供恢复角色状态的功能。
 * 优点：
 *      1. 封装性好，外部代码无需知道Memento的实现细节，只需要调用保存和恢复方法即可。
 *      2. 实现简单，Memento只需要保存角色的状态即可，无需复杂的序列化操作。
 *      3. 适应性广，Memento可以适用于任何需要保存和恢复角色状态的场景。
 *      4. 便于扩展，Memento可以根据需要增加或修改状态的保存和恢复逻辑。
 * 缺点：
 *      1. 保存和恢复过程复杂，需要考虑到各种异常情况，如角色状态保存失败、恢复失败等。
 *      2. 代码复杂，Memento需要实现复杂的序列化和反序列化逻辑。
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        log.info("大战Boss前...");
        GameRole gameRole = new GameRole();
        gameRole.initState();
        gameRole.showState();

        // 保存状态
        RoleStateCaretaker roleStateCaretaker = new RoleStateCaretaker();
        roleStateCaretaker.setMemento(gameRole.saveState());

        // 开始战斗
        log.info("大战Boss后...");
        gameRole.fight();
        gameRole.showState();

        log.info("恢复状态...");
        // 恢复状态
        gameRole.restoreState(roleStateCaretaker.getMemento());
        gameRole.showState();
    }
}
