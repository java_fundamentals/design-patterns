package com.design.behavioral.memento.blackbox;

/**
 * 备忘录对象管理类
 */
public class RoleStateCaretaker {
    //申明Memento对象
    private Memento memento;

    public Memento getMemento() {
        return memento;
    }
    public void setMemento(Memento memento) {
        this.memento = memento;
    }
}
