package com.design.behavioral.strategy;

/**
 * 策略类
 */
public interface Strategy {
    // 策略方法
    void show();
}
