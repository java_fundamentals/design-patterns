package com.design.behavioral.strategy;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class StrategyC implements Strategy{
    @Override
    public void show() {
        log.info("满1000加一元换购任意200以下商品");
    }
}
