package com.design.behavioral.strategy;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class StrategyA implements Strategy{
    @Override
    public void show() {
        log.info("买一送一");
    }
}
