package com.design.behavioral.strategy;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class StrategyB implements Strategy{
    @Override
    public void show() {
        log.info("满200减50");
    }
}
