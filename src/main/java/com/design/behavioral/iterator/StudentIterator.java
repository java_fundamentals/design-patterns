package com.design.behavioral.iterator;

/**
 * 抽象迭代器角色接口
 */
public interface StudentIterator {
    /**
     * 判断是否还有下一个元素
     */
    boolean hasNext();
    /**
     * 获取下一个元素
     *
     * @return
     */
    Student next();
}
