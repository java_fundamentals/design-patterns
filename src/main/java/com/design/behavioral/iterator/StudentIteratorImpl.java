package com.design.behavioral.iterator;

import java.util.List;

/**
 * 具体迭代器角色类
 */
public class StudentIteratorImpl implements StudentIterator{
    private List<Student> list;
    // 当前迭代器的索引
    private int index = 0;

    public StudentIteratorImpl(List<Student> list) {
        this.list = list;
    }
    @Override
    public boolean hasNext() {
        return index < list.size();
    }

    @Override
    public Student next() {
        // 从list中获取当前索引的元素
        Student student = list.get(index);
        // 索引加1
        index++;
        return student;
    }
}
