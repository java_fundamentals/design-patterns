package com.design.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

public class StudentAggregateImpl implements StudentAggregate {
    private List<Student> list = new ArrayList<>();
    @Override
    public void addStudent(Student student) {
        list.add(student);
    }

    @Override
    public void removeStudent(Student student) {
        list.remove(student);
    }

    // 返回一个迭代器对象，不同的聚合类可以返回不同的迭代器实现对象
    @Override
    public StudentIterator getIterator() {
        return new StudentIteratorImpl(list);
    }
}
