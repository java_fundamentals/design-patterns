package com.design.behavioral.iterator;

/**
 * 抽象聚合角色类
 */
public interface StudentAggregate {
    // 增加学生
    void addStudent(Student student);
    // 删除学生
    void removeStudent(Student student);
    // 获取学生迭代器
    StudentIterator getIterator();
}
