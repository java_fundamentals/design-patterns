package com.design.behavioral.iterator;

import lombok.extern.log4j.Log4j2;

/**
 * 迭代器模式
 * 定义：
 *      提供一个对象来顺序访问聚合对象中的一系列数据，而不暴露聚合对象的内部表示。
 * 适用场景：
 *      访问一个聚合对象的内容而无须暴露它的内部表示。
 * 优点：
 *      它支持以不同的方式遍历一个聚合对象，在同一个聚合对象上可以定义多个遍历。
 *      迭代器简化了聚合类。由于引入了迭代器，在迭代器类中只需要定义一个方法next即可，其他方法由迭代器来完成，在迭代器类中隐藏了聚合类内部表示，从而分离了迭代器遍历聚合的过程，即遍历算法与聚合对象的核心实现分离，满足“开闭原则”。
 * 缺点：
 *      由于迭代器将聚合类中核心遍历算法分离出来，因此如果聚合类中需要定义新的遍历算法，则需要修改迭代器类和聚合类，这增加了修改和维护的难度。
 * 使用场景：
 *      访问一个聚合对象的内容而无须暴露它的内部表示。
 *      需要为聚合对象提供多种遍历方式。
 *      为遍历不同的聚合结构提供一个统一的接口。
 *      为遍历中的聚合元素引入新的操作。
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        StudentAggregateImpl studentAggregate = new StudentAggregateImpl();
        studentAggregate.addStudent(new Student("Robert", "001"));
        studentAggregate.addStudent(new Student("Conor", "002"));
        studentAggregate.addStudent(new Student("John", "003"));
        // 创建迭代器
        StudentIterator iterator = studentAggregate.getIterator();
        // 遍历聚合对象
        while (iterator.hasNext()) {
            Student nextStudent = iterator.next();
            log.info("Student: [Name: {}, ID: {}]", nextStudent.getName(), nextStudent.getId());
        }
    }
}
