package com.design.behavioral.responsibility;

import lombok.extern.log4j.Log4j2;

/**
 * 部门经理类
 */
@Log4j2
public class Manager extends Handler{
    public Manager() {
        super(Handler.NUN_ONE,Handler.NUN_TWO);
    }

    @Override
    protected void handleRequest(LeaveRequset leaveRequset) {
        log.info("{}请假{}天,事由:{}。",leaveRequset.getName(),leaveRequset.getDays(),leaveRequset.getReason());
        log.info("部门经理审批：同意");
    }
}
