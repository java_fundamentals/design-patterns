package com.design.behavioral.responsibility;

import lombok.extern.log4j.Log4j2;

/**
 * 抽象处理者类
 */
@Log4j2
public abstract class Handler {
    protected final static int NUN_ONE = 1;
    protected final static int NUN_TWO = 3;
    protected final static int NUN_THREE = 7;

    // 该领导处理的请求天数区间
    private int numStart;
    private int numEnd;

    //申明后继者（上级领导）
    private Handler nextHandler;

    public Handler(int numStart) {
        this.numStart = numStart;
    }

    public Handler(int numStart, int numEnd) {
        this.numStart = numStart;
        this.numEnd = numEnd;
    }

    // 设置后继者（设置上级领导）
    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    // 处理请求的方法， 子类必须实现
    protected abstract void handleRequest(LeaveRequset leaveRequset);

    // 提交请假条
    public final void submitRequest(LeaveRequset leaveRequset) {
        // 如果该领导处理的请求天数区间包含该请求，则处理该请求
        this.handleRequest(leaveRequset);
        if (this.nextHandler != null && leaveRequset.getDays() > this.numEnd) {
            // 提交给上级领导进行审批处理
            this.nextHandler.submitRequest(leaveRequset);
        } else {
           log.info("流程结束");
        }
    }
}
