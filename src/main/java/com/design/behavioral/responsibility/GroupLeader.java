package com.design.behavioral.responsibility;

import lombok.extern.log4j.Log4j2;

/**
 * 小组长类
 */
@Log4j2
public class GroupLeader extends Handler{
    public GroupLeader() {
        super(0,Handler.NUN_ONE);
    }

    @Override
    protected void handleRequest(LeaveRequset leaveRequset) {
        log.info("{}请假{}天,事由:{}。",leaveRequset.getName(),leaveRequset.getDays(),leaveRequset.getReason());
        log.info("小组长审批：同意");
    }
}
