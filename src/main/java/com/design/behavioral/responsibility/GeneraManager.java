package com.design.behavioral.responsibility;

import lombok.extern.log4j.Log4j2;

/**
 * 总经理类
 */
@Log4j2
public class GeneraManager extends Handler{
    public GeneraManager() {
        super(Handler.NUN_TWO,Handler.NUN_THREE);
    }

    @Override
    protected void handleRequest(LeaveRequset leaveRequset) {
        log.info("{}请假{}天,事由:{}。",leaveRequset.getName(),leaveRequset.getDays(),leaveRequset.getReason());
        log.info("总经理审批：同意");
    }
}
