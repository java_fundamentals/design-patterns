package com.design.behavioral.template;

import lombok.extern.log4j.Log4j2;

/**
 * 模版模式
 * 定义：
 *      定义一个操作中的算法的骨架，而将一些步骤延迟到子类中。模板方法使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。
 * 关键代码：
 *      1、抽象类中，将算法中不同的步骤以具体方法或抽象方法的形式实现。
 *      2、在抽象类实现，子类实现。
 * 优点：
 *      1、封装不变部分，扩展可变部分。
 *      2、提取公共部分代码，便于维护。
 *      3、行为由父类控制，子类实现。
 * 缺点：
 *      每一个不同的实现都需要一个子类来实现，导致类的个数增加，使得系统更加庞大。
 * 使用场景：
 *      1、有多个子类共有的方法，且逻辑相同。
 *      2、重要的、复杂的方法，可以考虑作为模板方法。
 *      3、重构时，模板方法模式让子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。
 * 注意事项：
 *      为防止恶意操作，一般模板方法都加上final关键字。
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        AbstractClass concreteClass = new ConcreteClass_XiaoLongXia();
        concreteClass.cook();
        log.info("=====================");
        AbstractClass concreteClass2 = new ConcreteClass_BaoCai();
        concreteClass2.cook();
    }
}
