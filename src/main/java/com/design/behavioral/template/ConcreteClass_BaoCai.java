package com.design.behavioral.template;

import lombok.extern.log4j.Log4j2;

/**
 * 炒包菜类
 */
@Log4j2
public class ConcreteClass_BaoCai extends AbstractClass{
    @Override
    public void pourVegetable() {
        log.info("下锅的蔬菜是包菜");
    }

    @Override
    public void pourSauce() {
        log.info("下锅的酱料是蒜蓉");
    }
}
