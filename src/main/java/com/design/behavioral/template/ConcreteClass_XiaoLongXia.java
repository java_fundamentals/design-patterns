package com.design.behavioral.template;

import lombok.extern.log4j.Log4j2;

/**
 * 麻辣小龙虾类
 */
@Log4j2
public class ConcreteClass_XiaoLongXia extends AbstractClass{
    @Override
    public void pourVegetable() {
        log.info("下锅的是小龙虾");
    }

    @Override
    public void pourSauce() {
        log.info("下锅的汁是麻辣汁");
    }
}
