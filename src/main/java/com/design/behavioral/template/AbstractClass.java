package com.design.behavioral.template;

import lombok.extern.log4j.Log4j2;

/**
 * 抽象类（定义模版方法和基本方法）
 */
@Log4j2
public abstract class AbstractClass {

    //模板方法，确定方法的执行顺序
    public final void cook(){
        //第一步：开火
        openFire();
        //第二步：倒油
        pourOil();
        //第三步：热油
        heatOil();
        //第四步：倒蔬菜
        pourVegetable();
        //第五步：倒调味料
        pourSauce();
        //第六步：翻炒
        fry();
        //第七步：关火
        closeFire();
    }
    // 第一步：开火是一样的，所以直接实现
    public void openFire(){
        log.info("开火");
    }
    // 第二步：倒油是一样的，所以直接实现
    public void pourOil(){
        log.info("倒油");
    }
    // 第三步：热油是一样的，所以直接实现
    public void heatOil(){
        log.info("热油");
    }
    //第四步：倒蔬菜是不一样的
    public abstract void pourVegetable();
    //第五步：倒调味料是不一样的
    public abstract void pourSauce();
    //第六步：翻炒是一样的，所以直接实现
    public void fry() {
        log.info("烹饪中");
    }
    // 第七步：关火是一样的，所以直接实现
    public void closeFire(){
        log.info("关火");
    }
}
