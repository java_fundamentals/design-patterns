package com.design.structural.adapter.class_adapter;

import lombok.extern.log4j.Log4j2;

/**
 * SD卡的实现类
 */
@Log4j2
public class SDCardImpl implements SDCard{
    @Override
    public String readSD() {
        log.info("读取SD卡数据");
        return "Hello SDCard";
    }

    @Override
    public void writeSD(String msg) {
        log.info("写入SD卡数据：{}" ,msg);
    }
}
