package com.design.structural.adapter.class_adapter;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class SDAdapterTF extends TFCardImpl implements SDCard{
    @Override
    public String readSD() {
        log.info("adapter read tf card");
        return readTFCard();
    }

    @Override
    public void writeSD(String msg) {
        log.info("adapter write tf card");
        writeTFCard(msg);
    }
}
