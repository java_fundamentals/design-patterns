package com.design.structural.adapter.class_adapter;

/**
 * SDCard接口：需要适配的接口
 */
public interface SDCard {

    // 读取SD卡方法
    String readSD();

    // 写入SD卡方法
    void writeSD(String msg);
}
