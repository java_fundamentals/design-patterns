package com.design.structural.adapter.object_adapter;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class SDAdapterTF implements SDCard {
    //申明适配者类
    private TFCard tfCard;

    public SDAdapterTF(TFCard tfCard) {
        this.tfCard = tfCard;
    }
    @Override
    public String readSD() {
        log.info("adapter read tf card");
        return tfCard.readTFCard();
    }

    @Override
    public void writeSD(String msg) {
        log.info("adapter write tf card");
        tfCard.writeTFCard(msg);
    }
}
