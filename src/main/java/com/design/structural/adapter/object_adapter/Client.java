package com.design.structural.adapter.object_adapter;

import lombok.extern.log4j.Log4j2;

/**
 * 适配器模式-对象适配器
 * 定义：
 *      将一个类的接口转换成客户希望的另外一个接口。Adapter模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。
 *      适配器模式分为类结构型模式和对象结构型模式两种，前者类之间的耦合度比后者高，且要求程序员了解现有接口的内部结构，
 *      但灵活性好。
 * 主要角色：
 *      目标接口（Target）：当前系统业务所期待的接口，它可以是抽象类或接口。
 *      需要适配的类（Adaptee）：它是被访问和适配的现存组件库中的组件接口。
 *      适配器类（Adapter）：它是一个转换器，通过继承或引用适配者对象，把适配者接口转换成目标接口，让客户按目标接口的格式访问适配者。
 * 优点：
 *      可以让任何两个没有关联的类一起运行。
 *      提高了类的复用。
 *      增加了类的透明度。
 *      灵活性好。
 * 缺点：
 *      过多地使用适配器，会让系统非常零乱，不易整体进行把握。比如，明明看到调用的是A接口，其实内部被适配成了B接口的实现，一个系统如果太多出现这种情况，无异于一场灾难。
 *      由于JDK中没有提供适配器的实现，因此开发者需要自己实现，并且在类包级别上需要对其适配器进行注册管理，才能被外部调用，使用起来相当麻烦。
 * 使用场景：
 *      系统需要使用现有的类，而此类的接口不符合系统的需要。
 *      想要建立一个可以重复使用的类，用于与一些彼此之间没有太大关联的一些类，包括一些可能在将来引进的类一起工作，
 *      一个已经存在的类不能适应系统的要求，而你又不想对它进行修改的话。
 *      通过接口转换，将一个类插入另一个类系中。
 *      提供统一的接口，用来访问原来不能兼容的类。
 */

@Log4j2
public class Client {
    public static void main(String[] args) {
        // 使用适配器模式调用SD卡读取功能
        Computer computer = new Computer();
        SDCard sdCard = new SDCardImpl();
        log.info("SD卡的读取能力：" + computer.readSD(sdCard));
        // 使用该电脑读取TF卡中的数据
        TFCard tfCard = new TFCardImpl();
        log.info("TF卡的读取能力：" + computer.readSD(new SDAdapterTF(tfCard)));
    }
}
