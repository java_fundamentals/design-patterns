package com.design.structural.adapter.object_adapter;

import lombok.extern.log4j.Log4j2;

/**
 * 适配者类
 */
@Log4j2
public class TFCardImpl implements TFCard {
    @Override
    public String readTFCard() {
        String msg = "TFCard read msg: Hello, World!";
        log.info("TFCard read msg: {}", msg);
        return msg;
    }

    @Override
    public void writeTFCard(String msg) {
        log.info("TFCard write msg: {}", msg);
    }
}
