package com.design.structural.adapter.object_adapter;

/**
 * 适配者类的接口
 */
public interface TFCard {

    // 读取TFCard中的数据
    String readTFCard();

    // 写入数据到TFCard
    void writeTFCard(String msg);
}
