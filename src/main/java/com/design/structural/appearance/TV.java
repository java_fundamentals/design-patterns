package com.design.structural.appearance;

import lombok.extern.log4j.Log4j2;

/**
 * 电视节类
 */
@Log4j2
public class TV {

    public void turnOn(){
        log.info("打开电视");
    }

    public void turnOff(){
        log.info("关闭电视");
    }
}
