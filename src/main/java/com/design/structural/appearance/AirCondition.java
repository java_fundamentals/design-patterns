package com.design.structural.appearance;

import lombok.extern.log4j.Log4j2;

/**
 * 空调类
 */
@Log4j2
public class AirCondition {

    public void turnOn() {
        log.info("打开空调");
    }

    public void turnOff() {
        log.info("关闭空调");
    }
}
