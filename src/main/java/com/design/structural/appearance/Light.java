package com.design.structural.appearance;

import lombok.extern.log4j.Log4j2;

/**
 * 电灯类
 */
@Log4j2
public class Light {
    public void turnOn() {
        log.info("打开电灯");
    }

    public void turnOff() {
        log.info("关闭电灯");
    }
}
