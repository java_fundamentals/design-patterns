package com.design.structural.appearance;

import lombok.extern.log4j.Log4j2;

/**
 * 外观模式
 * 定义：
 *      为子系统中的一组接口提供一个一致的界面，此模式定义了一个高层接口，这个接口使得这一子系统更加容易使用。
 *      外观模式（Facade Pattern）是“外观模式”（Facade Pattern）的缩写，它是一种结构型模式，它向客户端提供了统一的访问入口，使得客户端无须了解各个模块之间的关系，使模块之间的解耦工作变得十分简单。
 * 外观模式的主要优点有：
 *       1、降低了客户端对子系统的依赖程度，并且提高了子系统的独立性和可移植性。
 *       2、对客户端屏蔽了子系统组件，减少了客户端处理的对象数目，并使得子系统使用起来更加容易。
 *       3、简化了客户端的代码，使得客户端更加容易使用。
 * 外观模式的主要缺点有：
 *       1、不能很好地限制客户端直接使用子系统类，如果对客户端访问子系统类做太多的限制则减少了可变性和灵活性
 *       2、如果设计不当，增加新的子系统可能需要修改外观类或客户端的源代码，违背了“开闭原则”
 * 外观模式的适用场景有：
 *       1、为复杂的模块或子系统提供外界访问的模块时。
 *       2、子系统相对独立时。
 *       3、预防低水平人员带来的风险时。
 * 外观模式的结构
 *       外观模式的主要角色如下。
 *       1.外观（Facade）角色：外观角色是一个过程型类，它被客户角色调用，在子系统之外处理对子系统的操作，在子系统之外完成，它与子系统的通信是通过Facade接口进行的。
 *       2.子系统（Sub System）角色：子系统角色是一个业务逻辑组件，它实现了子系统的功能。
 * 代码实现：
 *      本实例用到了外观模式、装饰模式和享元模式。首先，定义一个接口，该接口声明了子系统类所需要提供的方法。然后，创建一个实现了该接口的子系统类。最后，创建一个实现了外观接口的Facade类，该类持有子系统
 *      的一个实例，并实现了子系统接口。客户端需要与Facade对象进行通信，而非子系统类。
 */
@Log4j2
public class Client {
    public static void main(String[] args) {

        // 创建智能音箱对象
        SmartAppliancesFacade smartAppliancesFacade = new SmartAppliancesFacade();
        smartAppliancesFacade.say("全部打开家电");
        smartAppliancesFacade.say("全部关闭家电");
        log.info("========================");
        smartAppliancesFacade.say("打开空调");
        smartAppliancesFacade.say("打开电视");
        smartAppliancesFacade.say("打开客厅灯");

    }
}
