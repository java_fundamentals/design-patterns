package com.design.structural.appearance;

import lombok.extern.log4j.Log4j2;

/**
 * 外观类，用户主要和该类对象进行交互
 */
@Log4j2
public class SmartAppliancesFacade {
    // 聚合电器类对象
    private AirCondition airCondition;
    private TV tv;
    private Light light;

    public SmartAppliancesFacade() {
        this.airCondition = new AirCondition();
        this.tv = new TV();
        this.light = new Light();
    }

    // 提供给用户操作的方法
    public void say(String message){
        if (message.contains("全部打开")) {
            turnOn();
        } else if (message.contains("全部关闭")) {
            turnOff();
        } else if (message.contains("打开电视")) {
            turnOnTv();
        } else if (message.contains("关闭电视")) {
            turnOffTv();
        } else if (message.contains("打开空调")) {
            turnOnAirCondition();
        } else if (message.contains("关闭空调")) {
            turnOffAirCondition();
        } else if (message.contains("打开灯")) {
            turnOnLight();
        } else if (message.contains("关闭灯")) {
            turnOffLight();
        } else{
            log.info("对不起，我听不懂您的话，请重新输入！");
        }
    }

    /**
     * 一键打开功能
     */
    public void turnOn() {
        airCondition.turnOn();
        tv.turnOn();
        light.turnOn();
    }

    /**
     * 一键关闭功能
     */
    public void turnOff() {
        airCondition.turnOff();
        tv.turnOff();
        light.turnOff();
    }

    public void turnOnTv(){
        tv.turnOn();
    }

    public void turnOffTv(){
        tv.turnOff();
    }

    public void turnOnLight(){
        light.turnOn();
    }

    public void turnOffLight(){
        light.turnOff();
    }

    public void turnOnAirCondition(){
        airCondition.turnOn();
    }
    public void turnOffAirCondition(){
        airCondition.turnOff();
    }
}
