package com.design.structural.decorator;

/**
 * 培根类（具体的装饰者角色）
 */
public class Bacon extends Garnish{

    public Bacon(FastFood fastFood) {
        super(fastFood,2f,"培根");
    }

    @Override
    public float cost() {
        return getPrice()+getFastFood().cost();
    }
    @Override
    public String getDesc() {
        return super.getDesc()+"+"+super.getFastFood().getDesc();
    }
}
