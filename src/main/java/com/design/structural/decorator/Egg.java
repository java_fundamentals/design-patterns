package com.design.structural.decorator;

/**
 * 鸡蛋类（具体的装饰者角色）
 */
public class Egg extends Garnish{

    public Egg(FastFood fastFood) {
        super(fastFood,1.2f,"鸡蛋");
    }

    @Override
    public float cost() {
        // 返回被装饰者的价格加上鸡蛋的价格
        return getPrice()+getFastFood().cost();
    }
    @Override
    public String getDesc() {
        return super.getDesc()+"+"+super.getFastFood().getDesc();
    }
}
