package com.design.structural.decorator;

/**
 * 快餐类
 * 抽象构件角色
 */
public abstract class FastFood {
    // 价格
    private float price;
    // 描述
    private String desc;

    public FastFood(){

    }

    public FastFood(float price,String desc){
        this.price = price;
        this.desc = desc;
    }

    public abstract float cost();

    public float getPrice() {
        return price;
    }

    public String getDesc() {
        return desc;
    }

    public void setPrice(float price){
        this.price = price;
    }
}
