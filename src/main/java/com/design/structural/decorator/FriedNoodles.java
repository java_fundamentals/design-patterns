package com.design.structural.decorator;

/**
 * 具体构件角色：炒面
 */
public class FriedNoodles extends FastFood{

    public FriedNoodles(){
        super(11,"炒面");
    }
    @Override
    public float cost() {
        return getPrice();
    }
}
