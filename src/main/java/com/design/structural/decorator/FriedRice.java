package com.design.structural.decorator;

/**
 * 具体构件角色：炒饭
 */
public class FriedRice extends FastFood{
    // 构造器
    public FriedRice() {
        super(10, "炒饭");
    }

    // 返回价格
    @Override
    public float cost() {
        return getPrice();
    }
}
