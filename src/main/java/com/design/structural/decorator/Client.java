package com.design.structural.decorator;

import lombok.extern.log4j.Log4j2;

/**
 * 装饰者模式
 * 定义：
 *       动态地给一个对象添加一些额外的职责。就增加功能来说，装饰器模式相比生成子类更为灵活。
 * 角色：
 *       抽象构件（Component）角色：定义一个抽象接口以规范准备接收附加责任的对象。
 *       具体构件（Concrete Component）角色：实现抽象构件，通过装饰角色为其添加一些职责。
 *       抽象装饰（Decorator）角色：继承抽象构件，并包含具体构件的实例，可以通过其子类扩展具体构件的功能。
 *       具体装饰（Concrete Decorator）角色：实现抽象装饰的相关方法，并给具体构件对象添加附加的责任。
 *       客户端（Client）角色：通过装饰角色来扩展功能，并在需要的时候去掉装饰。
 * 优点：
 *       装饰类和被装饰类可以独立发展，不会相互耦合，装饰模式是继承的一个替代模式，装饰模式可以动态扩展一个对象的功能，通过配置文件可以在运行时选择不同的装饰器，从而实现不同的行为。
 *       装饰模式是继承的一个替代模式，装饰模式可以动态扩展一个对象的功能，通过配置文件可以在运行时选择不同的装饰器，从而实现不同的行为。
 * 缺点：
 *       多层装饰比较复杂。
 * 使用场景：
 *       需要动态地给一个对象增加功能，这些功能可以再动态地撤销。
 *       需要为一批的兄弟类进行改装或加装功能。
 *       需要设计一个具有层次的复合结构。
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        //点一份炒饭
        FastFood friedRice = new FriedRice();
        log.info("点一份炒饭，价格为：{}，描述为：{}", friedRice.cost(), friedRice.getDesc());
        //在炒饭中加入鸡蛋
        friedRice = new Egg(friedRice);
        log.info("加入鸡蛋，价格为：{}，描述为：{}", friedRice.cost(), friedRice.getDesc());
        //在炒饭中加入培根
        friedRice = new Bacon(friedRice);
        log.info("加入培根，价格为：{}，描述为：{}", friedRice.cost(), friedRice.getDesc());
        log.info("=======================================================");

        //点一份炒面
        FastFood noodles = new FriedNoodles();
        log.info("点一份炒面，价格为：{}，描述为：{}", noodles.cost(), noodles.getDesc());

        //在炒面中加入鸡蛋
        noodles = new Egg(noodles);
        log.info("加入鸡蛋，价格为：{}，描述为：{}", noodles.cost(), noodles.getDesc());

        //在炒面中加入火腿肠
        noodles = new HamSausage(noodles);
        log.info("加入火腿肠，价格为：{}，描述为：{}", noodles.cost(), noodles.getDesc());
        log.info("=======================================================");

        FastFood noodles1 = new FriedNoodles();
        noodles1 = new HamSausage(noodles1);
        noodles1 = new Bacon(noodles1);
        log.info("点一份炒面加入火腿肠、培根，价格为：{}，描述为：{}", noodles1.cost(), noodles1.getDesc());
    }
}
