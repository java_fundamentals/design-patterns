package com.design.structural.decorator;

/**
 * 火腿肠类（具体的装饰者角色）
 */
public class HamSausage extends Garnish{

    public HamSausage(FastFood fastFood) {
        super(fastFood,1.5f,"火腿肠");
    }

    @Override
    public float cost() {
        return getPrice()+getFastFood().cost();
    }
    @Override
    public String getDesc() {
        return super.getDesc()+"+"+super.getFastFood().getDesc();
    }
}