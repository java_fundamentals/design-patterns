package com.design.structural.proxy.static_proxy;

import lombok.extern.log4j.Log4j2;

/**
 * 静态代理
 * 定义：
 *      代理类和委托类实现了同一个接口，在代理类中调用委托类的方法，从而实现对委托类的代理和使用。
 * 优点：
 *      可以做到在符合开闭原则的情况下对目标对象进行功能扩展。
 * 缺点：
 *      代理类和委托类实现了同一个接口，代理类中除了委托类的方法外，还添加了其他的方法，因此会产生过多的代码，增加了系统实现的复杂度。
 * 使用场景：
 *      按职责来划分，通常有以下使用场景：
 *      1、远程代理。
 *      2、虚拟代理。
 *      3、Copy-on-Write 代理。
 *      4、保护（Protect or Access）代理。
 *      5、Cache代理。
 *      6、防火墙（Firewall）代理。
 *      7、同步化（Synchronization）代理。
 *      8、智能引用（Smart Reference）代理。
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        //创建代售点类对象
        ProxyPoint proxyPoint = new ProxyPoint();
        //通过代售点类对象调用方法
        proxyPoint.sell();
        log.info("销售完毕！");
    }
}
