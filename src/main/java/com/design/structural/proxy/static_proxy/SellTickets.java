package com.design.structural.proxy.static_proxy;

/**
 * 买票接口
 */
public interface SellTickets {
    void sell();
}
