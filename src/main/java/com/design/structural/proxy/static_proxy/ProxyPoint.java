package com.design.structural.proxy.static_proxy;

import lombok.extern.log4j.Log4j2;

/**
 * 代售点类
 */
@Log4j2
public class ProxyPoint implements SellTickets{
    // 申明火车站类
    private TrainStation trainStation = new TrainStation();

    @Override
    public void sell() {
        log.info("代售点收取服务费");
        trainStation.sell();
    }
}
