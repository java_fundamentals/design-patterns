package com.design.structural.proxy.jdk_proxy;

/**
 * 买票接口
 */
public interface SellTickets {
    void sell();
}
