package com.design.structural.proxy.jdk_proxy;

import lombok.extern.log4j.Log4j2;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 获取代理对象的工厂类
 * 代理类也实现了对应的接口
 */
@Log4j2
public class ProxyFactory {
    //申明目标对象
    private TrainStation trainStation = new TrainStation();
    public SellTickets getProxyObject(){
        // 获取代理对象
        SellTickets proxyObject = (SellTickets) Proxy.newProxyInstance(
                // 类加载器，用于加载代理类。可以通过目标对象获取类加载器
                trainStation.getClass().getClassLoader(),
                // 代理类实现的接口的字节码对象
                trainStation.getClass().getInterfaces(),
                //代理对象的调用处理程序
                new InvocationHandler() {
                    /**
                     *
                     * @param proxy 代理对象。和proxyObject对象是同一个对象，在invoke方法中基本不用
                     *
                     * @param method 对接口中的方法进行封装的method对象
                     *
                     * @param args 调用方法的实际参数
                     *
                     * @return 为方法的返回值
                     * @throws Throwable
                     */
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        log.info("invoke方法执行了");
                        log.info("代售点收取一定的服务费(jdk动态代理)");
                        log.info("代理类调用的方法是："+method.getName());
                        //执行目标对象的方法
                        Object obj = method.invoke(trainStation, args);
                        log.info("invoke方法执行完毕");
                        return obj;
                    }
                }
        );
        return proxyObject;
    }
}
