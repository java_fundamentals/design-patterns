package com.design.structural.proxy.cglib_proxy;

import lombok.extern.log4j.Log4j2;

/**
 * 火车站类
 */
@Log4j2
public class TrainStation{
    public void sell() {
        log.info("火车站卖票");
    }
}
