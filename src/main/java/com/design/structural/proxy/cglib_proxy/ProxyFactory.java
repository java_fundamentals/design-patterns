package com.design.structural.proxy.cglib_proxy;

import lombok.extern.log4j.Log4j2;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

/**
 * 代理对象工厂，用来获取代理对象
 */
@Log4j2
public class ProxyFactory {
    private Object target;
    private MethodInterceptor interceptor;

    private ProxyFactory(Builder builder) {
        this.target = builder.target;
        this.interceptor = builder.interceptor;
    }

    public static class Builder {
        private Object target;
        private MethodInterceptor interceptor;

        public Builder target(Object target) {
            this.target = target;
            return this;
        }

        public Builder interceptor(MethodInterceptor interceptor) {
            this.interceptor = interceptor;
            return this;
        }

        public ProxyFactory build() {
            return new ProxyFactory(this);
        }
    }



    public TrainStation getProxyObject() {
        //创建Enhancer对象，类似于JDK动态代理的Proxy类，下一步就是设置几个参数
        Enhancer enhancer =new Enhancer();

        //设置代理对象的父类的字节码对象(Class类型的对象) , 指定代理对象的父类
        enhancer.setSuperclass(target.getClass());

        //设置回调函数 , 实现调用代理对象的方法时最终都会执行MethodInterceptor的子实现类的intercept方法 , 在这个函数中利用反射完成任意目标类方法的调用
        enhancer.setCallback(interceptor);

        //设置完参数后就可以 ,默认返回的是Object类型 , 可以进行强转 , 创建真正的代理对象
        TrainStation proxyObject = (TrainStation) enhancer.create();
        return proxyObject;
    }

}