package com.design.structural.proxy.cglib_proxy;

import lombok.extern.log4j.Log4j2;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import java.lang.reflect.Method;

@Log4j2
public class MyMethodInterceptor implements MethodInterceptor {
    // intercept方法参数说明:
    //		返回值类型是调用方法的返回值类型
    //		o ： 代理对象
    //		method ： 真实对象中的方法的Method实例对象
    //		args ： 实际参数 , 可以是0到N个
    //		methodProxy ：代理对象中的方法的method实例
    @Override
    public Object intercept(Object obj, Method method, Object[] args,
                            MethodProxy proxy) throws Throwable {
        log.info("Before method: Intercepting " + method.getName());
        Object result = proxy.invokeSuper(obj, args);
        log.info("After method: Done intercepting " + method.getName());
        return result;
    }

}
