package com.design.structural.proxy.cglib_proxy;

/**
 * cglib 动态代理
 * 定义：
 *        cglib是一个强大的高性能的代码生成包，它可以在运行期扩展java类与实现java接口。它广泛的被许多AOP的框架
 *        所使用，例如Spring AOP和synaop，为他们提供方法的interception（拦截）。
 *        cglib包的底层是通过使用一个小而快的字节码处理框架ASM，来转换字节码并生成新的类。
 *        除了CGLIB以外，类似cglib这样的框架还有：Javassist
 * 特点：
 *        1.字节码实现，JDK动态代理是反射机制实现。
 *        2.CGLIB是通过继承的方式做的动态代理，因此如果某个类被标记为final，那么它是无法使用CGLIB做代理的。
 *        3.CGLIB比JDK动态代理的效率要高，但是CGLIB在创建代理对象时所花费的时间却比JDK动态代理多得多，
 *           因此，对于单例类、Final类或者无实例构造函数的类，CGLIB不适合做动态代理。
 * 使用：
 *        1.引入cglib的jar文件：cglib-3.3.0.jar。
 *        2.在内存中动态构建子类实例：Enhancer类。
 *        3.Enhancer通过使用字节码处理框架ASM，对目标类进行继承扩展，并动态生成目标类的子类，
 *           通过方法拦截的技术，将横切逻辑代码加到子类目标方法的前后。
 * 优点：
 *        1.在运行期动态的创建目标类的子类对象，从而实现了在运行期动态的创建目标类对象。
 *        2.不要求目标类必须实现一个接口。
 * 缺点：
 *       1.由于在运行期动态创建了目标类的子类对象，所以可能会影响系统的性能。
 *       2.由于使用继承的方式，所以不能对final类进行代理。
 * 使用场景：
 *        1.需要通过代理类对目标类进行扩展。
 *        2.需要通过代理类对目标类进行装饰。
 * @author liuc
 */
public class CglibClient {
    public static void main(String[] args) {
        //创建代理工厂对象
        ProxyFactory factory = new ProxyFactory.Builder()
                //设置目标对象
                .target(new TrainStation())
                //设置拦截器
                .interceptor(new MyMethodInterceptor())
                .build();
        //获取代理对象
        TrainStation proxyObject = factory.getProxyObject();

        proxyObject.sell();
    }
}
