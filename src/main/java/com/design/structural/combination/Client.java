package com.design.structural.combination;

/**
 * 组合模式：分为透明和安全两种实现方式。
 * 透明方式：组合对象和叶子对象实现相同的接口。
 * 安全方式：组合对象只实现部分接口，叶子对象实现全部接口。
 *
 * 定义：
 *      将对象组合成树形结构以表示“部分-整体”的层次结构。组合模式使得用户对单个对象和组合对象的使用具有一致性。
 * 适用场景：
 *      表示对象的部分-整体层次结构（树形结构）。
 *      希望用户忽略组合对象与单个对象的不同，用户将统一地使用组合结构中的所有对象。
 * 优点：
 *      1.高层模块调用简单。
 *      2.节点自由增加。
 * 缺点：
 *      1.节点自由增加，导致节点类数量增加，系统复杂度提高。
 *      2.需要为每个节点类设计一个接口或基类，当组合树过深时，遍历树的算法复杂度较高。
 *      3.不适合使用在叶子对象很多的情况，如果叶子对象很多，会导致组合树节点数量过多，此时应考虑使用其他模式，
 *          如：享元模式、代理模式。
 * 使用场景：
 *      1.表示对象的部分-整体层次结构（树形结构）。
 *      2.希望用户忽略组合对象与单个对象的不同，用户将统一地使用组合结构中的所有对象。
 *      3.处理的对象具有树形结构特点，即存在父级对象和子级对象的关系。
 *      4.需要表示对象的部分-整体层次结构，但用户无须关心它们。
 * 注意事项：
 *      1.节点类应该只有一个抽象基类，如果为每个节点类都创建一个接口或基类，会导致系统类的数量过多。
 *      2.为叶子对象和组合对象提供公共接口。
 *      3.为叶子对象和组合对象提供默认实现。
 *      4.为组合对象添加管理子对象的方法。
 *      5.如果节点类很多，可建立一个抽象根节点类，它用于存储和管理子节点对象，避免节点类中包含和管理子节点对象的相关代码。
 *      6.可建立一个父节点类，它用于存储和管理子节点对象，避免叶子对象中包含和管理子节点对象的相关代码。
 */
public class Client {
    public static void main(String[] args) {
        // 创建菜单树
        MenuComponent menu1 = new Menu("菜单管理", 2);
        menu1.add(new MenuItem("页面访问", 3));
        menu1.add(new MenuItem("展开菜单", 3));
        menu1.add(new MenuItem("编辑菜单", 3));
        menu1.add(new MenuItem("删除菜单", 3));
        menu1.add(new MenuItem("新增菜单", 3));
        MenuComponent menu2 = new Menu("权限管理", 2);
        menu2.add(new MenuItem("页面访问", 3));
        menu2.add(new MenuItem("提交保存", 3));
        MenuComponent menu3 = new Menu("角色管理", 2);
        menu3.add(new MenuItem("页面访问", 3));
        menu3.add(new MenuItem("新增角色", 3));
        menu3.add(new MenuItem("修改角色", 3));

        // 构建菜单树
        MenuComponent rootMenu = new Menu("系统管理", 1);
        rootMenu.add(menu1);
        rootMenu.add(menu2);
        rootMenu.add(menu3);

        // 打印菜单
        rootMenu.print();
    }
}
