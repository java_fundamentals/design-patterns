package com.design.structural.combination;

import lombok.extern.log4j.Log4j2;

/**
 * 菜单项类：叶子对象
 */
@Log4j2
public class MenuItem extends MenuComponent{
    public MenuItem(String name,int level) {
        this.name = name;
        this.level = level;
    }

    @Override
    public void print() {
        for (int i = 0; i < level; i++) {
            System.out.print("-");
        }
        System.out.println(name);
    }
}
