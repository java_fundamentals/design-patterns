package com.design.structural.combination;

import lombok.extern.log4j.Log4j2;
import java.util.ArrayList;
import java.util.List;

/**
 * 菜单类：组合类
 */
@Log4j2
public class Menu extends MenuComponent{
    // 菜单可以有多个子菜单或者子菜单项
    private List<MenuComponent> menuComponentList = new ArrayList<>();

    // 构造方法
    public Menu(String name, int level) {
        this.name = name;
        this.level = level;
    }

    @Override
    public void add(MenuComponent menuComponent) {
        menuComponentList.add(menuComponent);
    }

    @Override
    public void remove(MenuComponent menuComponent) {
        menuComponentList.remove(menuComponent);
    }

    @Override
    public MenuComponent getChild(int i) {
        return menuComponentList.get(i);
    }

    @Override
    public void print() {

        // 打印菜单名称
        for (int i = 0; i < level; i++) {
            System.out.print("-");
        }
        System.out.println(name);

        // 打印子菜单或子菜单项
        for (MenuComponent menuComponent : menuComponentList) {
            menuComponent.print();
        }
    }
}
