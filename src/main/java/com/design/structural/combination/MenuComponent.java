package com.design.structural.combination;

/**
 * 菜单组件：属于抽象根节点
 */
public abstract class MenuComponent {
    //菜单组件的名称
    protected String name;
    //菜单组件的层级
    protected int level;
    //添加子菜单
    public void add(MenuComponent menuComponent){
        throw new UnsupportedOperationException();
    }
    //移除子菜单
    public void remove(MenuComponent menuComponent){
        throw new UnsupportedOperationException();
    }
    //获取子菜单
    public MenuComponent getChild(int i){
        throw new UnsupportedOperationException();
    }
    //获取菜单名称
    public String getName(){
        return name;
    }

    //打印菜单名称（包括子菜单）
    public abstract void print();
}
