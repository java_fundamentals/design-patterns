package com.design.structural.flyweight;

import lombok.extern.log4j.Log4j2;

/**
 * 抽象享元类
 */
@Log4j2
public abstract class AbstractBox {

    //所有的具体享元类必须实现的抽象方法，
    //返回一个不重复的标识
    public abstract String getShape();

    //显示颜色
    public void display(String color){
        log.info("方块形状:{} ,颜色:{}",getShape(),color);
    }
}
