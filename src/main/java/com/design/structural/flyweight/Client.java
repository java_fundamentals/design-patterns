package com.design.structural.flyweight;

import lombok.extern.log4j.Log4j2;

/**
 * 享元模式
 * 定义：
 *      享元模式是一种用于性能优化的模式，享元模式的核心是运用共享技术来有效支持大量细粒度对象的复用。
 * 主要解决：
 *      系统中有大量对象，这些对象消耗大量内存，而且这些对象的状态大部分可以外部化，也就是说可以通过参数进行设置，那么就可以考虑使用享元模式
 *      或者工厂模式来减少内存中的对象数量，或readyonly、cache等技术来减少相同对象重复创建的次数。
 * 何时使用：
 *      1、系统中有大量对象。
 *      2、这些对象消耗大量内存。
 *      3、这些对象的状态大部分可以外部化。
 *      4、这些对象可以按照内蕴状态分为很多组，当把外蕴对象从对象中剔除出来时，每一组对象都可以用一个对象来代替。
 *      5、系统不依赖于这些对象身份，这些对象是不可分辨的。
 * 关键代码：
 *      用HashMap来存储已创建的对象。
 *      为外部状态创建一个缓冲池。
 * 应用实例：
 *      1、JAVA中的String，如果有则返回，如果没有则创建一个字符串保存在字符串缓存池里面。
 *      2、数据库的数据池。
 * 优点：
 *      大大减少对象的创建，降低系统的内存，使效率提高。
 * 缺点：
 *      提高了系统的复杂度，需要分离出外部状态和内部状态，而且外部状态具有固有化的性质，不应该随着内部状态的变化而变化，否则会造成系统的混乱。
 * 使用场景：
 *      1、系统有大量相似对象。
 *      2、需要缓冲池的场景。
 *      3、访问共享资源时，需要加锁。
 *      4、需要优化性能的时候。
 * 注意事项：
 *      1、注意划分外部状态和内部状态，否则可能会引起线程安全问题。
 *      2、这些类必须有一个工厂对象加以控制。
 *      3、这些类需要实现可序列化接口。
 *
 */
@Log4j2
public class Client {
    public static void main(String[] args) {
        //获取I图形对象
        AbstractBox iBox = BoxFactory.getInstance().getShape("I");
        iBox.display("红色");

        //获取L图形对象
        AbstractBox lBox = BoxFactory.getInstance().getShape("L");
        lBox.display("黑色");

        //获取O图形对象
        AbstractBox oBox = BoxFactory.getInstance().getShape("O");
        oBox.display("蓝色");

        //获取O图形对象
        AbstractBox oBox1 = BoxFactory.getInstance().getShape("O");
        oBox1.display("灰色");

        log.info("两次获取到的O对象是否是同一个："+ (oBox == oBox1));
    }
}
