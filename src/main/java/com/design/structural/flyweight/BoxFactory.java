package com.design.structural.flyweight;

import java.util.HashMap;

/**
 * 工厂类，将该类设计为单利类
 */
public class BoxFactory {
    // 创建一个map，用于存储
    private static HashMap<String, AbstractBox> map;

    //在构造方法中进行初始化操作
    private BoxFactory(){
        map = new HashMap<>();
        //创建一个L形
        map.put("L", new LBox());
        //创建一个I形
        map.put("I", new IBox());
        //创建一个O形
        map.put("O", new OBox());
    }

    // 提供一个方法获取该工厂类对象
    public static BoxFactory getInstance(){
        return BoxFactoryHolder.instance;
    }


    // 创建一个静态内部类
    private static class BoxFactoryHolder{
        private static final BoxFactory instance = new BoxFactory();
    }

    //根据名称获取图形对象
    public static AbstractBox getShape(String name){
        return map.get(name);
    }

}
