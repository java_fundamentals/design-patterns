package com.design.structural.bridge;

import lombok.extern.log4j.Log4j2;

/**
 * 扩展抽象化角色（Mac系统）
 */
@Log4j2
public class Mac extends OpratingSystem{
    public Mac(VideoFile videoFile) {
        super(videoFile);
    }

    @Override
    public void play(String fileName) {
        log.info("Mac系统下播放视频");
        videoFile.decode(fileName);
    }
}
