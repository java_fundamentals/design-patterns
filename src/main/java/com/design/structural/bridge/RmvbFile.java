package com.design.structural.bridge;

import lombok.extern.log4j.Log4j2;

/**
 * RMVB文件类：具体实现化角色
 */
@Log4j2
public class RmvbFile implements VideoFile{
    @Override
    public void decode(String fileName) {
        log.info("rmvb 视频文件：" + fileName);
    }
}
