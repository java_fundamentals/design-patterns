package com.design.structural.bridge;

import lombok.extern.log4j.Log4j2;

/**
 * avi视频文件（具体的实现化角色）
 */
@Log4j2
public class AviFile implements VideoFile{
    @Override
    public void decode(String fileName) {
        log.info("avi视频文件："+fileName);
    }
}
